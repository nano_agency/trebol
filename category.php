<?php
/**
 * The template for displaying Category pages
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */
$title_cat      = get_theme_mod('trebol_post_title_heading',true);
if(isset($_GET['title'])){
        $title_cat=$_GET['title'];
}

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <section class="wrap-breadcrumb">
            <div class="container">  
                <?php if ( $title_cat ) : ?>
                        <?php do_action('category-title'); ?>                
                        <?php trebol_woocommerce_breadcrumb(); ?>
                <?php endif;?>
            </div>
        </section>
        <div class="container">
            <div class="content-category">
                <div class="row">
                    <?php do_action('archive-sidebar-left'); ?>
                    <?php do_action('archive-content-before'); ?>
                    <?php if ( have_posts() ) : ?>
                        <div class="archive-blog">
                            <div class="row">
                                <div class="affect-isotope">
                                    <?php get_template_part( 'loop' );?>
                                </div>
                            </div>
                        </div>
                    <?php else :                    
                        get_template_part( 'content', 'none' );
                    endif; ?>
                    <?php
                    the_posts_pagination( array(
                        'prev_text'          => '<i class="ion-ios-arrow-thin-left icons"></i>',
                        'next_text'          => '<i class="ion-ios-arrow-thin-right icons"></i>',
                        'before_page_number' => '<span class="meta-nav screen-reader-text"></span>',
                    ) );
                    ?>
                    <?php do_action('archive-content-after'); ?>
                    <?php do_action('archive-sidebar-right'); ?>
                </div>
            </div>    
        </div>
    </main>
</div>

<?php get_footer();