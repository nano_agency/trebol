<?php
/**
 * The template for displaying Category pages
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

get_header(); 
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
    	<header class="page-header text-center">
			<h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'bizi' ), get_search_query() ); ?></h1>
		</header>                
		<div class="container">
			<div class="content-category">
				<div class="row">
					<?php do_action('archive-sidebar-left'); ?>
					<?php do_action('archive-content-before'); ?>
					<?php if ( have_posts() ) : ?>						
						<div class="archive-blog">
							<div class="row">
								<div class="affect-isotope">
									<?php get_template_part( 'loop' );?>
								</div>								
							</div>
						</div>
					<?php else :					
						get_template_part( 'content', 'none' );
					endif; ?>
					<?php
						the_posts_pagination( array(
							'prev_text'          => '<i class="ion-ios-arrow-thin-left icons"></i>',
							'next_text'          => '<i class="ion-ios-arrow-thin-right icons"></i>',
							'before_page_number' => '<span class="meta-nav screen-reader-text"></span>',
						) );
					?>
					<?php do_action('archive-content-after'); ?>
					<?php do_action('archive-sidebar-right'); ?>					
				</div>
			</div>
		</div>
    </main>
</div>

<?php get_footer();

