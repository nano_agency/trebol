<?php
/**
 * The template for displaying Category pages
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

$layout_content      = get_theme_mod('trebol_cat_content_layout', 'list');
$content_col         = get_theme_mod('trebol_cat_content_col', '1');

	if ($content_col && $content_col==='2'){
		$class='col-md-6 col-sm-6 col-xs-12';
	} elseif ($content_col && $content_col==='3'){
		$class='col-md-4 col-sm-6 col-xs-12';
	} else{
		$class='col-md-12 col-sm-12 col-xs-12';
	}

//get url
	if(isset($_GET['col'])){
		$class=$_GET['col'];
	}
	if(isset($_GET['des'])){
		$content_des=$_GET['des'];
	}
	if(isset($_GET['content'])){
		$layout_content=$_GET['content'];
	}

$na=1;
// Start the Loop.
while ( have_posts() ) : the_post(); 
	
	if($layout_content=='masonry'){
		if($na%2==0){?>
			<div class="item-post item-post-masonry col-item <?php echo esc_attr($class);?>">
				<?php get_template_part( 'templates/layout/content-masonry'); ?>
			</div>
		<?php } else{ ?>
			<div class="item-post item-post-masonry col-item <?php echo esc_attr($class);?>">
				<?php get_template_part( 'templates/layout/content-grid'); ?>
			</div>
		<?php }	?>
	<?php }elseif ($layout_content=='list'){?>
        <div class="item-post col-item col-md-12 col-lg-12">
            <?php get_template_part( 'templates/layout/content-list'); ?>
        </div>
    <?php }
	else{ ?>
		<div class="item-post col-item <?php echo esc_attr($class);?>">
			<?php get_template_part( 'templates/layout/content' ,$layout_content); ?>
		</div>
<?php } $na++; endwhile;

?>
