<form method="get"  class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="input-group">
        <label class="screen-reader-text"><?php echo esc_attr__( 'Search for:','trebol' ); ?></label>
        <input type="text" class="form-control" placeholder="<?php echo esc_attr__( 'Search ... ','trebol' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
        <span class="input-group-btn">
            <button class="button btn-search">
            	<span><?php echo esc_html__( 'Search','trebol' ); ?></span>
            	<i class="icon-magnifier icons"></i>
            </button>
        </span>
    </div>
</form>