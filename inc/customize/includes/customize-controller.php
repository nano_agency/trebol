<?php
/**
 * @package     NA Core
 * @version     0.1
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */
if (!class_exists('trebol_Customize')) {
    class trebol_Customize
    {
        public $customizers = array();

        public $panels = array();

        public function init()
        {
            $this->customizer();
            add_action('customize_controls_enqueue_scripts', array($this, 'trebol_customizer_script'));
            add_action('customize_register', array($this, 'trebol_register_theme_customizer'));
            add_action('customize_register', array($this, 'remove_default_customize_section'), 20);
        }

        public static function &getInstance()
        {
            static $instance;
            if (!isset($instance)) {
                $instance = new trebol_Customize();
            }
            return $instance;
        }

        protected function customizer()
        {
            $this->panels = array(

                'blog_panel' => array(
                    'title'             => esc_html__('Blog','trebol'),
                    'description'       => esc_html__('Blog >','trebol'),
                    'priority'          =>  101,
                ),
                'sidebar_panel' => array(
                    'title'             => esc_html__('Sidebar','trebol'),
                    'description'       => esc_html__('Sidebar Setting','trebol'),
                    'priority'          => 103,
                ),
                'trebol_option_panel' => array(
                    'title'             => esc_html__('Option','trebol'),
                    'description'       => '',
                    'priority'          => 104,
                ),
            );

            $this->customizers = array(
                'title_tagline' => array(
                    'title' => esc_html__('Site Identity', 'trebol'),
                    'priority'  =>  1,
                    'settings' => array(
                        'trebol_logo' => array(
                            'class' => 'image',
                            'label' => esc_html__('Logo', 'trebol'),
                            'description' => esc_html__('Upload Logo Image', 'trebol'),
                            'priority' => 12
                        ),
                    )
                ),
//2.General ============================================================================================================
            'trebol_general' => array(
            'title' => esc_html__('General', 'trebol'),
            'description' => '',
            'priority' => 2,
            'settings' => array(

                'trebol_bg_body' => array(
                    'label'         => esc_html__('Background - Body', 'trebol'),
                    'description'   => '',
                    'class'         => 'color',
                    'priority'      => 2,
                    'params'        => array(
                        'default'   => '',
                    ),
                    
                ),
                'trebol_primary_body' => array(
                    'label'         => esc_html__('Primary - Color', 'trebol'),
                    'description'   => '',
                    'class'         => 'color',
                    'priority'      => 1,
                    'params'        => array(
                        'default'   => '',
                    ),
                    
                ),
                'trebol_cart' => array(
                    'class' => 'toggle',
                    'label' => esc_html__('Enable Cart','trebol'),
                    'priority' => 4,
                    'params' => array(
                        'default' => false,
                    ),
                ),
            )
        ),
//3.Header =============================================================================================================
                'trebol_header' => array(
                    'title' => esc_html__('Header', 'trebol'),
                    'description' => '',
                    'priority' => 3,
                    'settings' => array(
                        //header
                        'trebol_header_heading' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Header', 'trebol'),
                            'priority' => 0,
                        ),
                        'trebol_header' => array(
                            'type' => 'select',
                            'label' => esc_html__('Header Layout', 'trebol'),
                            'description' => '',
                            'priority' => 1,
                            'choices' => array(
                                'simple'            => esc_html__('Default', 'trebol'),
                                'full'              => esc_html__('Full Width', 'trebol'),
                                'vertical'          => esc_html__('Vertical', 'trebol'),
                            ),
                            'params' => array(
                                'default' => 'simple',
                            ),
                        ),

                        'trebol_keep_menu' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Menu Keep', 'trebol'),
                            'priority' =>4,
                            'params' => array(
                                'default' => true,
                            ),
                            
                        ),
                        'trebol_bg_header' => array(
                            'label'         => __('Background - Header', 'trebol'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 5,
                            'params'        => array(
                                'default'   => '',
                            ),
                            
                        ),

                        'trebol_color_menu' => array(
                            'label'         => __('Color - Text', 'trebol'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 6,
                            'params'        => array(
                                'default'   => '',
                            ),                            
                        ),

                        //topbar
                        'trebol_topbar_heading' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Top bar', 'trebol'),
                            'priority' => 7,
                        ),

                        'trebol_topbar_config' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Enable Topbar', 'trebol'),
                            'priority' => 8,
                            'params' => array(
                                'default' => false,
                            ),                            
                        ),

                        'trebol_bg_topbar' => array(
                            'label'         => __('Background - Top bar', 'trebol'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 9,
                            'params'        => array(
                                'default'   => '',
                            ),
                            
                        ),
                        'trebol_color_topbar' => array(
                            'label'         => __('Color - Text ', 'trebol'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 10,
                            'params'        => array(
                                'default'   => '',
                            ),
                            
                        ),

                    )
                ),
//4.Footer =============================================================================================================
                'trebol_new_section_footer' => array(
                    'title' => __('Footer', 'trebol'),
                    'description' => '',
                    'priority' => 4,
                    'settings' => array(
                        'trebol_footer' => array(
                            'type' => 'select',
                            'label' => __('Choose Footer Style', 'trebol'),
                            'description' => '',
                            'priority' => -1,
                            'choices' => array(
                                'footer-1'     => __('Footer 1', 'trebol'),
                                'footer-2'     => __('Footer 2', 'trebol'),
                                'footer-hidden'        => __('Hidden Footer', 'trebol')
                            ),
                            'params' => array(
                                'default' => 'footer-1',
                            ),
                            
                        ),

                        'trebol_bg_footer' => array(
                            'label'         => __('Background - Footer', 'trebol'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 2,
                            'params'        => array(
                                'default'   => '',
                            ),
                            
                        ),

                        'trebol_color_footer' => array(
                            'label'         => __('Color - Text ', 'trebol'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 3,
                            'params'        => array(
                                'default'   => '',
                            ),
                            
                        ),

                        'trebol_copyright_text' => array(
                            'type' => 'textarea',
                            'label' => __('Footer Copyright Text', 'trebol'),
                            'description' => '',
                            'priority' => 0,
                            
                        ),

                    )
                ),

//5.Font  ===============================================================================================================
                'trebol_new_section_font_size' => array(
                    'title' => __('Font', 'trebol'),
                    'description' => 'Font Option',
                    'priority' => 5,
                    'settings' => array(

                        'trebol_body_font_google' => array(
                            'type'          => 'select',
                            'label'         => __('Use Google Font', 'trebol'),
                            'choices'       => trebol_googlefont(),
                            'priority'      => 1,
                            'params'        => array(
                                'default'       => 'Poppins',
                            ),
                        ),

                        'trebol_body_font_size' => array(
                            'type'          => 'number',
                            'label'         => __('Body Font Size', 'trebol'),
                            'description'   => '',
                            'priority'      => 2,
                            'params'        => array(
                                'default'       => '',
                            ),
                        ),
                        'trebol_menu_font_size' => array(
                            'type'          => 'number',
                            'label'         => __('Menu Font Size', 'trebol'),
                            'description'   => '',
                            'priority'      =>3,
                            'params'        => array(
                                'default'    => '',
                            ),
                        ),
                    ),
                ),

//6.Categories Blog ====================================================================================================
                'trebol_blog' => array(
                    'title' => esc_html__('Blogs Categories', 'trebol'),
                    'description' => '',
                    'panel'        =>'blog_panel',
                    'priority' => 6,
                    'settings' => array(

                        'trebol_sidebar_cat' => array(
                            'class'         => 'layout',
                            'label'         => esc_html__('Sidebar Layout', 'trebol'),
                            'priority'      =>3,
                            'choices'       => array(
                                'left'         => get_template_directory_uri().'/assets/images/left.png',
                                'right'        => get_template_directory_uri().'/assets/images/right.png',
                                'full'         => get_template_directory_uri().'/assets/images/full.png',
                            ),
                            'params' => array(
                                'default' => 'full',
                            ),
                        ),
                        'trebol_siderbar_cat_info' => array(
                            'class' => 'info',
                            'label' => esc_html__('Info', 'trebol'),
                            'description' => esc_html__( 'Please goto Appearance > Widgets > drop drag widget to the sidebar Article.', 'trebol' ),
                            'priority' => 4,
                        ),

                        //layout content
                        'trebol_cat_content_heading' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Content', 'trebol'),
                            'priority' =>19,
                        ),
                        'trebol_post_title_heading' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Category Title','trebol'),
                            'priority' => 20,
                            'params' => array(
                                'default' => true,
                            ),
                        ),
                        'trebol_cat_content_layout' => array(
                            'class'         => 'layout',
                            'priority'      =>21,
                            'choices'       => array(
                                'grid'    => get_template_directory_uri().'/assets/images/box-grid.jpg',
                                'list'    => get_template_directory_uri().'/assets/images/box-list.jpg',  
                                'masonry' => get_template_directory_uri().'/assets/images/box-masonry.jpg',                                                                
                            ),
                            'params' => array(
                                'default' => 'list',
                            ),
                        ),
                        'trebol_cat_content_col' => array(
                            'class' => 'slider',
                            'label' => esc_html__('Number post on row ', 'trebol'),
                            'description' => '',
                            'priority' =>22,
                            'choices' => array(
                                'max' => 3,
                                'min' => 1,
                                'step' => 1
                            ),
                            'params'      => array(
                                'default' => 1
                            ),
                        ),
                        'trebol_post_meta_author' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Author','trebol'),
                            'priority' => 24,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'trebol_post_meta_date' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Date','trebol'),
                            'priority' => 24,
                            'params' => array(
                                'default' =>true,
                            ),

                        ),
                        
                    ),
                 ),
//7.Single blog ========================================================================================================
                'trebol_blog_single' => array(
                    'title' => esc_html__('Blog Single', 'trebol'),
                    'description' => '',
                    'panel'        =>'blog_panel',
                    'priority' => 7,
                    'settings' => array(
                        'trebol_sidebar_single' => array(
                            'class'    => 'layout',
                            'label'    => esc_html__('Sidebar Layout', 'trebol'),
                            'priority' =>13,
                            'choices'  => array(                                
                                'full' => get_template_directory_uri().'/assets/images/full.png',
                            ),
                            'params' => array(
                                'default' => 'right',
                            ),
                        ),
                        'trebol_siderbar_single_info' => array(
                            'class' => 'info',
                            'label' => esc_html__('Info', 'trebol'),
                            'description' => esc_html__( 'Please goto Appearance > Widgets > drop drag widget to the sidebar Article.', 'trebol' ),
                            'priority' => 14,
                        ),
                        //share
                        'trebol_single_share' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Share', 'trebol'),
                            'priority' =>15,
                        ),
                        'trebol_share_facebook' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Share Facebook  ','trebol'),
                            'priority' => 17,
                            'params' => array(
                                'default' => true,
                            ),
                            
                        ),
                        'trebol_share_twitter' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Share Twitter  ','trebol'),
                            'priority' => 18,
                            'params' => array(
                                'default' => true,
                            ),
                            
                        ),
                        'trebol_share_google' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Share Google  ','trebol'),
                            'priority' => 19,
                            'params' => array(
                                'default' => true,
                            ),
                            
                        ),

                        'trebol_share_linkedin' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Share Linkedin  ','trebol'),
                            'priority' => 20,
                            'params' => array(
                                'default' => false,
                            ),
                            
                        ),

                        'trebol_share_pinterest' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Share Pinterest  ','trebol'),
                            'priority' => 21,
                            'params' => array(
                                'default' => false,
                            ),                            
                        ),
                    ),
                ),

//8.Woocommerces  Category =============================================================================================
                'woocommerce_product_catalog' => array(
                    'title' => esc_html__('Product Catalog', 'trebol'),
                    'description' => '',
                    'panel' =>'woocommerce',
                    'priority' => 10,
                    'settings' => array(
                        //siderbar
                        'trebol_subcate_woo' => array(
                            'class'=> 'layout',
                            'label' => esc_html__('Category Layout:', 'trebol'),
                            'priority' =>98,
                            'choices' => array(
                                'cat-1'         => get_template_directory_uri().'/assets/images/cat-1.jpg',
                                'cat-2'         => get_template_directory_uri().'/assets/images/cat-2.jpg',
                                'cat-3'         => get_template_directory_uri().'/assets/images/cat-3.jpg',
                                'cat-4'         => get_template_directory_uri().'/assets/images/cat-3.jpg',
                            ),
                            'params' => array(
                                'default' => 'cat-2',
                            ),
                        ),
                        'trebol_subcate_info' => array(
                            'class' => 'info',
                            'label' => esc_html__('Info', 'trebol'),
                            'description' => esc_html__( 'Choose what to display Category layout  on the main shop page.', 'trebol' ),
                            'priority' => 99,
                        ),
                        //layout
                        'trebol_header_layout_cate' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Shop Layouts', 'trebol'),
                            'priority' =>100,
                        ),
                        'trebol_layout_woocat' => array(
                            'type' => 'select',
                            'label' => esc_html__('Shop Layouts', 'trebol'),
                            'description' => '',
                            'priority' => 100,
                            'choices' => array(
                                'container'         => esc_html__('Container', 'trebol'),
                                'container-fluid'   => esc_html__('Container Fluid', 'trebol'),
                                'full-width'        => esc_html__('Full Width', 'trebol'),
                                
                            ),
                            'params' => array(
                                'default' => 'container',
                            ),
                        ),
                        'trebol_top_shop' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Disable Top Shop', 'trebol'),
                            'priority' =>100,
                            'params' => array(
                                'default' => false,
                            ),
                        ),  
                        'trebol_top_shop_info' => array(
                            'class' => 'info',
                            'label' => esc_html__('Info', 'trebol'),
                            'description' => esc_html__( 'include: Breadcrumb and Header Title', 'trebol' ),
                            'priority' => 100,
                        ),
                        //category
                        'trebol_header_woocat' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Categories Product', 'trebol'),
                            'priority' =>100,
                        ),
                        //Filter
                        'trebol_filter_woo' => array(
                            'class'=> 'layout',
                            'label' => esc_html__('Filter Layout', 'trebol'),
                            'priority' =>101,
                            'choices' => array(
                                'left'                  => get_template_directory_uri().'/assets/images/shop/filter-left.jpg',
                                'full'                  => get_template_directory_uri().'/assets/images/shop/filter-full.jpg',
                                'down'                  => get_template_directory_uri().'/assets/images/shop/filter-down.jpg',
                                'sidebar-left'          => get_template_directory_uri().'/assets/images/left.png',
                            ),
                            'params' => array(
                                'default' => 'left',
                            ),
                        ),

                        'trebol_siderbar_info' => array(
                            'class' => 'info',
                            'label' => esc_html__('Info', 'trebol'),
                            'description' => esc_html__( 'Please goto Appearance > Widgets > drop drag widget to the sidebar SHOP.', 'trebol' ),
                            'priority' => 102,
                        ),
                        'trebol_woo_layered_nav' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Enable Layered Nav Filters', 'trebol'),
                            'priority' =>103,
                            'params' => array(
                                'default' => false,
                            ),
                        ),        
                        'trebol_woo_product_per_row' => array(
                            'class' => 'slider',
                            'label' => esc_html__('Products per row', 'trebol'),
                            'description' => '',
                            'priority' =>108,
                            'choices' => array(
                                'max' => 6,
                                'min' => 1,
                                'step' => 1
                            ),
                            'params'      => array(
                                'default' => 4
                            ),
                        ),
                        'trebol_woo_product_per_page' => array(
                            'class' => 'slider',
                            'label' => esc_html__('Number products per page', 'trebol'),
                            'description' => '',
                            'priority' =>109,
                            'choices' => array(
                                'max' => 36,
                                'min' => 9,
                                'step' => 1
                            ),
                            'params'      => array(
                                'default' => 24
                            ),
                        ),
                        'trebol_woo_pagination' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Enable Load More', 'trebol'),
                            'priority' =>110,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'trebol_custom_nav_check' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Use Customize List Category', 'trebol'),
                            'priority' =>110,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'trebol_custom_info' => array(
                            'class' => 'info',
                            'label' => esc_html__('Info', 'trebol'),
                            'description' => esc_html__( 'You can input html for the list categories or MENU  goto > Menu > tick to  Shop Navigation .', 'trebol' ),
                            'priority' => 111,
                        ),
                        'trebol_custom_nav' => array(
                            'type' => 'textarea',
                            'label' => __('Customize List Category', 'trebol'),
                            'description' => '',
                            'priority' => 111,
                        ),
                    ),
                ),
//9.Woocommerces Product Layouts'  =====================================================================================
                'trebol_woo_product' => array(
                    'title' => esc_html__('Product Layouts', 'trebol'),
                    'description' => '',
                    'panel' =>'woocommerce',
                    'priority' => 9,
                    'settings' => array(
                        //siderbar
                        'trebol_layout_product' => array(
                            'class'=> 'layout',
                            'label' => esc_html__('Layouts', 'trebol'),
                            'priority' =>1,
                            'choices' => array(
                                'grid'         => get_template_directory_uri().'/assets/images/box-grid.jpg',
                                'list'         => get_template_directory_uri().'/assets/images/box-list.jpg',
                                'trans'         => get_template_directory_uri().'/assets/images/box-tran.jpg',
                                'gallery'      => get_template_directory_uri().'/assets/images/box-gallery.jpg',
                            ),
                            'params' => array(
                                'default' => 'grid',
                            ),
                        ),
                        'trebol_space_product' => array(
                            'type' => 'select',
                            'label' => __('Choose space between of the product', 'trebol'),
                            'description' => '',
                            'priority' => 1,
                            'choices' => array(
                                'no-padding'  => esc_html('0'),
                                'padding5px'  => esc_html('5px'),
                                'padding10px'  => esc_html('10px'),
                                'padding15px'  => esc_html('15px'),
                                'padding20px'  => esc_html('20px'),
                            ),
                            'params' => array(
                                'default' => 'padding5px',
                            ),
                        ),

                        'trebol_woo_product_sale_flash' => array(
                            'type'          => 'checkbox',
                            'label'         => esc_html__('Show Discount', 'trebol'),
                            'priority'      => 2,
                            'params'        => array(
                                'default'   => false,
                            ),

                        ),

                        'trebol_woo_cart' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Add To Cart','trebol'),
                            'priority' => 3,
                            'params' => array(
                                'default' => false,
                            ),
                        ),
                        'trebol_woo_share_twitter' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Twitter','trebol'),
                            'priority' => 27,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'trebol_woo_share_pinterest' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Pinterest','trebol'),
                            'priority' => 28,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'trebol_woo_share_google' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Google +','trebol'),
                            'priority' => 29,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'trebol_woo_share_linkedin' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('linkedin','trebol'),
                            'priority' => 30,
                            'params' => array(
                                'default' => false,
                            ),

                        ),

                    ),
                ),
//10.Woocommerces Detail  ===============================================================================================
                'trebol_woo_detail' => array(
                    'title' => esc_html__('Product Detail', 'trebol'),
                    'description' => '',
                    'panel' =>'woocommerce',
                    'priority' => 10,
                    'settings' => array(
                        //siderbar
                        'trebol_sidebar_woo_single' => array(
                            'class'=> 'layout',
                            'label' => esc_html__('Sidebar', 'trebol'),
                            'priority' =>2,
                            'choices' => array(
                                'left'         => get_template_directory_uri().'/assets/images/left.png',
                                'full'         => get_template_directory_uri().'/assets/images/full.png',
                                'right'         => get_template_directory_uri().'/assets/images/right.png',
                            ),
                            'params' => array(
                                'default' => 'full',
                            ),
                        ),
                        'trebol_woo_related_products' => array(
                            'type'          => 'checkbox',
                            'label'         => __('Display Related Products', 'trebol'),
                            'priority'      => 3,
                            'params'        => array(
                                'default'   => true,
                            ),

                        ),
                        'trebol_woo_number_related_products' => array(
                            'type' => 'select',
                            'label' => __('Number Products Show', 'trebol'),
                            'description' => '',
                            'priority' => 4,
                            'choices' => array(
                                '3' => '3',
                                '4' => '4',
                                '5' => '5',
                                '6' => '6',
                            ),
                            'params' => array(
                                'default' => '4',
                            ),
                        ),
                        'trebol_woo_recently_viewed' => array(
                            'type'          => 'checkbox',
                            'label'         => __('Display Recently Viewed Products', 'trebol'),
                            'priority'      => 5,
                            'params'        => array(
                                'default'   => false,
                            ),

                        ),
                        //Detail layouts
                        'trebol_detail_layouts_heading' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Detail Layouts', 'trebol'),
                            'priority' =>10,
                        ),
                        'trebol_detail_layouts' => array(
                            'class'=> 'layout',
                            'label' => esc_html__('Layouts', 'trebol'),
                            'priority' =>11,
                            'choices' => array(
                                'vertical'              => get_template_directory_uri().'/assets/images/shop/detail-vertical.jpg',
                                'horizontal'            => get_template_directory_uri().'/assets/images/shop/detail-horizontal.jpg',
                                'grid'                  => get_template_directory_uri().'/assets/images/shop/detail-grid.jpg',
                                'carousel'              => get_template_directory_uri().'/assets/images/shop/detail-carousel.jpg',
                                'carousel-sticky'        => get_template_directory_uri().'/assets/images/shop/detail-carousel-sticky.jpg',
                                'carousel-left'        => get_template_directory_uri().'/assets/images/shop/detail-carousel-left.jpg',
                                'background'        => get_template_directory_uri().'/assets/images/shop/detail-carousel-background.jpg',                                
                            ),
                            'params' => array(
                                'default' => 'vertical',
                            ),
                        ),
                        'trebol_detail_bg_entry' => array(
                            'label'         => esc_html__('Background - Entry Summary', 'trebol'),
                            'description'   => '',
                            'class'         => 'color',
                            'priority'      => 12,
                            'params'        => array(
                                'default'   => '',
                            ),
                        ),
                        'trebol_detail_bg_entry_info' => array(
                            'class' => 'info',
                            'label' => esc_html__('Info', 'trebol'),
                            'description' => esc_html__( 'Background Entry Summary only run when using Carousel Left and Background layout (last layout)', 'trebol' ),
                            'priority' => 13,
                        ),    
                        //Share products
                        'trebol_cat_content_heading' => array(
                            'class' => 'heading',
                            'label' => esc_html__('Share', 'trebol'),
                            'priority' =>25,
                        ),
                        'trebol_woo_share_facebook' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Facebook','trebol'),
                            'priority' => 26,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'trebol_woo_share_twitter' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Twitter','trebol'),
                            'priority' => 27,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'trebol_woo_share_pinterest' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Pinterest','trebol'),
                            'priority' => 28,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'trebol_woo_share_google' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('Google +','trebol'),
                            'priority' => 29,
                            'params' => array(
                                'default' => false,
                            ),

                        ),
                        'trebol_woo_share_linkedin' => array(
                            'class' => 'toggle',
                            'label' => esc_html__('linkedin','trebol'),
                            'priority' => 30,
                            'params' => array(
                                'default' => false,
                            ),

                        ),

                    ),
                ),
            );
        }

        public function trebol_customizer_script()
        {
            // Register
            wp_enqueue_style('trebol-customize', get_template_directory_uri() . '/inc/customize/assets/css/customizer.css', array(),null);
            wp_enqueue_style('jquery-ui', get_template_directory_uri() . '/inc/customize/assets/css/jquery-ui.min.css', array(),null);
            wp_enqueue_script('trebol-customize', get_template_directory_uri() . '/inc/customize/assets/js/customizer.js', array('jquery'), null, true);
        }

        public function add_customize($customizers) {
            $this->customizers = array_merge($this->customizers, $customizers);
        }


        public function trebol_register_theme_customizer()
        {
            global $wp_customize;

            foreach ($this->customizers as $section => $section_params) {

                //add section
                $wp_customize->add_section($section, $section_params);
                if (isset($section_params['settings']) && count($section_params['settings']) > 0) {
                    foreach ($section_params['settings'] as $setting => $params) {

                        //add setting
                        $setting_params = array();
                        if (isset($params['params'])) {
                            $setting_params = $params['params'];
                            unset($params['params']);
                        }
                        $wp_customize->add_setting($setting, array_merge( array( 'sanitize_callback' => null ), $setting_params));
                        //Get class control
                        $class = 'WP_Customize_Control';
                        if (isset($params['class']) && !empty($params['class'])) {
                            $class = 'WP_Customize_' . ucfirst($params['class']) . '_Control';
                            unset($params['class']);
                        }

                        //add params section and settings
                        $params['section'] = $section;
                        $params['settings'] = $setting;

                        //add controll
                        $wp_customize->add_control(
                            new $class($wp_customize, $setting, $params)
                        );
                    }
                }
            }

            foreach($this->panels as $key => $panel){
                $wp_customize->add_panel($key, $panel);
            }

            return;
        }

        public function remove_default_customize_section()
        {
            global $wp_customize;
//            // Remove Sections
            $wp_customize->remove_control('woocommerce_catalog_columns');
            $wp_customize->remove_section('header_image');
            $wp_customize->remove_section('nav');
            $wp_customize->remove_section('static_front_page');
            $wp_customize->remove_section('colors');
            $wp_customize->remove_section('background_image');
        }
    }
}