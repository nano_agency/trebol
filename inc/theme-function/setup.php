<?php
/**
 * @package     trebol
 * @version     1.0
 * @author      NanoAgency
 * @link        http://www.nanoagency.co
 * @copyright   Copyright (c) 2016 NanoAgency
 * @license     GPL v2
 */

/*  Setup Theme ===================================================================================================== */
add_action( 'after_setup_theme', 'trebol_theme_setup' );
if ( ! function_exists( 'trebol_theme_setup' ) ) :
    function trebol_theme_setup() {
        load_theme_textdomain( 'trebol', get_template_directory() . '/languages' );

        //  Add default posts and comments RSS feed links to head.
        add_theme_support( 'automatic-feed-links' );

        //  Let WordPress manage the document title.
        add_theme_support( 'title-tag' );

        //  Enable support for Post Thumbnails on posts and pages.
        add_theme_support( 'post-thumbnails' );

        set_post_thumbnail_size( 825, 510, true );

        add_image_size( 'thumb-image', 600, 450, true);

        //Enable support for Post Formats.
        add_theme_support( 'html5', array(
            'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
        ) );

        add_theme_support( 'post-formats', array(
            'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
        ) );

        add_theme_support( 'custom-header' );

        add_theme_support( 'custom-background' );

        add_theme_support( "title-tag" );

        add_theme_support( 'woocommerce' );
    }
endif;

/* Thumbnail Sizes ================================================================================================== */
set_post_thumbnail_size( 220, 150, true);

//layout blog
add_image_size( 'trebol-blog-list', 1170 ,500, true);
add_image_size( 'trebol-blog-grid', 737 ,637, true); 
add_image_size( 'trebol-blog-masonry', 360 ,637, true);
add_image_size( 'trebol-blog-sidebar', 100, 87, true);

//layout category
add_image_size( 'trebol-category-thumb', 462, 503,true );
add_image_size( 'trebol-category-thumb-large', 698, 503,true );
add_image_size( 'trebol-category-thumbs-wide', 580, 310,true );

//layout detail
add_image_size( 'trebol-product-vertical-thumbnail', 84, 99,true );
add_image_size( 'trebol-product-vertical', 767, 905,true );
add_image_size( 'trebol-product-horizontal', 767, 905,true );
add_image_size( 'trebol-product-horizontal-thumbnail', 102, 120,true );
add_image_size( 'trebol-product-sticky', 1000, 1180,true );
add_image_size( 'trebol-product-grid', 295, 350,true );
add_image_size( 'trebol-product-large', 1045, 855,true );
add_image_size( 'trebol-product-carousel', 930, 742,true );
add_image_size( 'trebol-product-background', 652, 770,true );
add_image_size( 'trebol-product-background-thumbnail', 100, 118,true );

//layout shop
add_image_size( 'trebol-galerry-thumbnail-size', 800, 800,true );


/* Setup Font ======================================================================================================= */
function trebol_font_url() {
    $fonts_url = '';
    $poppins    = _x( 'on', 'Poppins font: on or off', 'trebol' );    

    if ( 'off' !== $poppins ) {
        $font_families = array();

        if ( 'off' !== $poppins) {
            $font_families[] = 'Poppins:300,400,500,600,700';
        }
        
        $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext' ),
        );

        $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
    }

    return esc_url_raw( $fonts_url );
}


/* Load Front-end scripts  ========================================================================================== */
add_action( 'wp_enqueue_scripts', 'trebol_theme_scripts');
function trebol_theme_scripts() {

    // Add  fonts, used in the main stylesheet.
    wp_enqueue_style( 'trebol-fonts', trebol_font_url(), array(), null );

    //style bootstrap
    wp_enqueue_style('bootstrap-css',get_template_directory_uri().'/assets/css/bootstrap.min.css', array(), '3.0.2 ');

    wp_enqueue_style('photoswipe-css',get_template_directory_uri().'/assets/css/photoswipe.css', array(), null);
    wp_enqueue_style('photoswipe-default-skin',get_template_directory_uri().'/assets/css/photoswipe-skin/default-skin.css', array(), null);

    wp_enqueue_style('jquery-ui', get_template_directory_uri() . '/inc/customize/assets/css/jquery-ui.min.css', array(),null);
    //style MAIN THEME
    wp_enqueue_style( 'trebol-main', get_template_directory_uri(). '/style.css', array(), null );

    //style skin
    wp_enqueue_style('trebol-css', get_template_directory_uri().'/assets/css/style-default.min.css' );

    //register all plugins
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri().'/assets/js/plugins/bootstrap.min.js', array(), '2.2.0', true );
    wp_enqueue_script( 'html5-js', get_template_directory_uri().'/assets/js/plugins/html5.min.js', array(), '2.2.0', true );
    wp_enqueue_script( 'skip-link-focus', get_template_directory_uri().'/assets/js/plugins/skip-link-focus-fix.min.js', array(), '2.2.0', true );
    wp_enqueue_script( 'lazy-load', get_template_directory_uri().'/assets/js/plugins/jquery.lazy.js', array(), '2.2.0', true ); //lazy load
    wp_enqueue_script( 'slick-js', get_template_directory_uri().'/assets/js/plugins/slick.min.js', array(), '2.2.0', true ); //slick
    wp_enqueue_script( 'accordion-menu', get_template_directory_uri().'/assets/js/plugins/accordion-menu.js', array(), null, true ); //accordion-menu

    wp_enqueue_script('jquery-masonry');
    wp_enqueue_script( 'isotope-js', get_template_directory_uri().'/assets/js/plugins/isotope.pkgd.min.js', array(), '2.2.0', true );
    wp_enqueue_script( 'imagesloaded-js', get_template_directory_uri().'/assets/js/plugins/imagesloaded.pkgd.min.js', array(), '2.2.0', true );

    wp_register_script( 'parallax-js', get_template_directory_uri().'/assets/js/plugins/parallax.min.js', array(), '1.1.3', true ); //parallax
    wp_register_script( 'countdown-timer-js', get_template_directory_uri().'/assets/js/plugins/jquery.countdown.min.js', array(), '2.2.0', true );


    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
    if ( is_singular() && wp_attachment_is_image() ) {
        wp_enqueue_script( 'trebol-theme-keyboard-image-navigation', get_template_directory_uri() . '/assets/js/plugins/keyboard-image-navigation.min.js', array( 'jquery' ), '20141010' );
    }
    if( function_exists('is_product') && is_product()){
        wp_enqueue_script( 'wc-add-to-cart-variation' ); //woo
        wp_enqueue_script( 'photoswipe', get_template_directory_uri().'/assets/js/plugins/photoswipe.min.js', array(), null, true );
        wp_enqueue_script( 'photoswipe-ui-default', get_template_directory_uri().'/assets/js/plugins/photoswipe-ui-default.min.js', array(), null, true );
        wp_register_script( 'sticky-sidebar-js', get_template_directory_uri().'/assets/js/plugins/jquery.sticky-sidebar.js', array(), '1.4.0', true );
        wp_register_script( 'slick-init', get_template_directory_uri().'/assets/js/dev/slick-init.js', array('jquery'),null, true );
    }
    //jquery MAIN THEME
    wp_enqueue_script('isotope-init', get_template_directory_uri() . '/assets/js/dev/isotope-init.js', array('jquery'),null, true);
    wp_enqueue_script('trebol', get_template_directory_uri() . '/assets/js/dev/trebol.js', array('jquery'),null, true);

}

/* Load Back-end SCRIPTS============================================================================================= */
function trebol_js_enqueue()
{
    wp_enqueue_media();
    wp_enqueue_style('thickbox');
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    // moved the js to an external file, you may want to change the path
    wp_enqueue_script('information_js',get_template_directory_uri(). '/assets/js/plugins/widget.min.js', 'jquery', '1.0', true);
}
add_action('admin_enqueue_scripts', 'trebol_js_enqueue');

/* Register the required plugins    ================================================================================= */
add_action( 'tgmpa_register', 'trebol_register_required_plugins' );
function trebol_register_required_plugins() {

    $plugins = array(
        // This is an example of how to include a plugin pre-packaged with a theme.
        array(
            'name'      => esc_html__( 'Trebol Core Plugin', 'trebol' ),
            'slug'      => 'trebol-core',
            'source'    => esc_url('http://guide.nanoagency.co/data/over/trebol-core.zip'),
            'required'  => true,
            'version'   => '1.0.0',
            'force_activation' => false,
            'force_deactivation' => false,
            'image_url' => get_template_directory_uri() . '/inc/backend/assets/images/plugins/nano.jpg',

        ),
        //Contact form 7
        array(
            'name'      => esc_html__('Contact Form 7', 'trebol' ),
            'slug'      => 'contact-form-7',
            'required'  => false,
            'image_url' => get_template_directory_uri() . '/inc/backend/assets/images/plugins/contact-form7.jpg',
        ),
        //MailChimp for WordPress
        array(
            'name'      =>  esc_html__('MailChimp for WordPress ', 'trebol' ),
            'slug'      => 'mailchimp-for-wp',
            'required'  => false,
            'image_url' => get_template_directory_uri() . '/inc/backend/assets/images/plugins/mailchimp.jpg',
        ),
        //WPBakery Visual Composer
        array(
            'name'      =>  esc_html__('WPBakery Visual Compose', 'trebol' ),
            'slug'      => 'js_composer',
            'source'    => esc_url('http://guide.nanoagency.co/data/plugins/js_composer.zip'),
            'required'  => true,
            'version'   => '5.0.1',
            'image_url' => get_template_directory_uri() . '/inc/backend/assets/images/plugins/vc.jpg',
        ),

        //Instagram
        array(
            'name'      =>  esc_html__('Instagram Feed', 'trebol' ),
            'slug'      => 'instagram-feed',
            'required'  => false,
            'image_url' => get_template_directory_uri() . '/inc/backend/assets/images/plugins/instagram.jpg',
        ),
        //woocommerce
        array(
            'name'      =>  esc_html__('WooCommerce', 'trebol' ),
            'slug'      => 'woocommerce',
            'required'  => false,
            'image_url' => get_template_directory_uri() . '/inc/backend/assets/images/plugins/woo.jpg',
        ),
    );

    $config = array(
        'id'           => 'trebol',                   // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                       // Default absolute path to pre-packaged plugins.
        'has_notices'  => true,
        'menu'         => 'tgmpa-install-plugins',  // Menu slug.
        'dismiss_msg'  => '',                       // If 'dismissable' is false, this message will be output at top of nag.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'is_automatic' => true,                     // Automatically activate plugins after installation or not.
        'message'      => '',                       // Message to output right before the plugins table.
    );

    tgmpa( $plugins, $config );

}

/* Register Navigation ============================================================================================== */
register_nav_menus( array(
    'primary_navigation'    => esc_html__( 'Primary Navigation', 'trebol' ),
    'shop_navigation'        => esc_html__( 'Shop Navigation', 'trebol' ),
) );

/* Register Sidebar ================================================================================================= */
if ( function_exists('register_sidebar') ) {
    register_sidebar( array(
        'name'          => esc_html__( 'Archive', 'trebol' ),
        'id'            => 'archive',
        'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'trebol' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Shop', 'trebol' ),
        'id'            => 'shop',
        'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'trebol' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar(array(
        'name' => esc_html__('Footer column 1','trebol'),
        'id'   => 'footer1-column1',
        'before_widget' => '<div id="%1$s" class="widget first %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => esc_html__('Footer column 2 ','trebol'),
        'id'   => 'footer1-column2',
        'before_widget' => '<div id="%1$s" class="widget first %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => esc_html__('Footer column 3 ','trebol'),
        'id'   => 'footer1-column3',
        'before_widget' => '<div id="%1$s" class="widget first %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => esc_html__('Footer column 4','trebol'),
        'id'   => 'footer1-column4',
        'before_widget' => '<div id="%1$s" class="widget first %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => esc_html__('Footer Bottom Left','trebol'),
        'id'   => 'footer-bottom-left',
        'before_widget' => '<div id="%1$s" class="widget first %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    )); 
    register_sidebar(array(
        'name' => esc_html__('Footer Bottom Right','trebol'),
        'id'   => 'footer-bottom-right',
        'before_widget' => '<div id="%1$s" class="widget first %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer 2  column 1','trebol'),
        'id'   => 'footer2-column1',
        'before_widget' => '<div id="%1$s" class="widget first %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));
    register_sidebar(array(
        'name' => esc_html__('Footer 2  column 2','trebol'),
        'id'   => 'footer2-column2',
        'before_widget' => '<div id="%1$s" class="widget first %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Custom Intro Sidebar','trebol'),
        'id'   => 'custom-intro-sidebar',
        'before_widget' => '<div id="%1$s" class="widget last %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Custom Topbar','trebol'),
        'id'   => 'custom-topbar',
        'before_widget' => '<div id="%1$s" class="widget last %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));    
}