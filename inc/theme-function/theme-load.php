<?php
/**
 * @package     trebol
 * @version     1.0
 * @author      NanoAgency
 * @link        http://www.nanoagency.co
 * @copyright   Copyright (c) 2016 NanoAgency
 * @license     GPL v2
 */

/*  Include template-tags =========================================================================================== */
require_once( get_template_directory() . '/templates/template-tags.php');
/*  Include Customize Menu ========================================================================================== */
require_once(get_template_directory() . '/templates/header/mega-menu.php');

/*  Include Customizer ============================================================================================== */
require_once( get_template_directory() .'/inc/customize/customize.php');
require_once(get_template_directory() . '/inc/customize/customize-style.php');

/*  Include Widgets ================================================================================================= */
require_once(get_template_directory() .'/inc/widgets/social.php');
require_once(get_template_directory() .'/inc/widgets/search.php');
require_once(get_template_directory() .'/inc/widgets/about.php');
require_once(get_template_directory() .'/inc/widgets/recent-post.php');
require_once(get_template_directory() .'/inc/widgets/contact-info.php');
if (in_array('woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ))) {
    require_once(get_template_directory() . '/inc/widgets/widget-layered.php');
    require_once(get_template_directory() . '/inc/widgets/woocommerce-price-filter.php');
    require_once(get_template_directory() . '/inc/widgets/wcva_add_layered_navigation_widget.php');
}