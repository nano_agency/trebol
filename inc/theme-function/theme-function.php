<?php
/**
 * @package     trebol
 * @version     1.0
 * @author      NanoAgency
 * @link        http://www.nanoagency.co
 * @copyright   Copyright (c) 2016 NanoAgency
 * @license     GPL v2
 */

/* Customize font Google  =========================================================================================== */
if(!function_exists('trebol_googlefont')){
    function trebol_googlefont(){
        global $wp_filesystem;
        $filepath = get_template_directory().'/assets/googlefont/googlefont.json';
        if( empty( $wp_filesystem ) ) {
            require_once( ABSPATH .'/wp-admin/includes/file.php' );
            WP_Filesystem();
        }
        if( $wp_filesystem ) {
            $listGoogleFont=$wp_filesystem->get_contents($filepath);
        }
        if($listGoogleFont){
            $gfont = json_decode($listGoogleFont);
            $fontarray = $gfont->items;
            $options = array();
            foreach($fontarray as $font){
                $options[$font->family] = $font->family;
            }
            return $options;
        }
        return false;
    }
}

/* Post Thumbnail =================================================================================================== */
if ( ! function_exists( 'trebol_post_thumbnail' ) ) :
    function trebol_post_thumbnail() {
        if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
            return;
        }

        if ( is_singular() ) :
            ?>

            <div class="post-thumbnail">
                <?php the_post_thumbnail(); ?>
            </div><!-- .post-thumbnail -->

        <?php else : ?>

            <a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
                <?php the_post_thumbnail( 'post-thumbnail', array( 'alt' => the_title_attribute( 'echo=0' ) ) ); ?>
            </a>

        <?php endif; // End is_singular()
    }
endif;

/* Excerpt more  ==================================================================================================== */
function trebol_new_excerpt_more($more)
{
    return '...';
}
add_filter('excerpt_more', 'trebol_new_excerpt_more');

if(!function_exists('trebol_string_limit_words')){
    function trebol_string_limit_words($string, $word_limit)
    {
        $words = explode(' ', $string, ($word_limit + 1));

        if(count($words) > $word_limit) {
            array_pop($words);
        }

        return implode(' ', $words);
    }
}

function trebol_excerpt( $class = 'entry-excerpt' ) {
    if ( has_excerpt() || is_search() ) : ?>
        <div class="<?php echo esc_attr( $class ); ?>">
            <?php the_excerpt(); ?>
        </div><!-- .<?php echo esc_attr( $class ); ?> -->
    <?php endif;
}
/* add body class ====================================================================================================*/
add_filter( 'body_class', 'trebol_class' );
function trebol_class( $classes ) {
    global  $post;
    //check layout header
    $class_header = get_theme_mod('trebol_header', 'full');
    if(is_page()){
        $class_header = get_post_meta($post->ID, 'layout_header', true);
    }
    if ( $class_header) {
        $classes[] = 'trebol-header-'.$class_header;
    }
    return $classes;
}
/* TopBar ============================================================================================================*/
add_action( 'trebol_topbar', 'trebol_topbar_config');
if(!function_exists('trebol_topbar_config')){
    function trebol_topbar_config() {
        global  $post;
        $configTopbar               = get_theme_mod('trebol_topbar_config',false);
        if(is_page()){
            $configTopbar           = get_post_meta(get_the_ID(), 'trebol_show_topbar',true);
        }
        if(isset($configTopbar) && $configTopbar != '0' && !empty($configTopbar)){?>
            <div class="header-topbar">
                <?php  get_template_part('templates/topbar'); ?>
            </div>
        <?php }
    }
}

/* Header ============================================================================================================*/
add_action( 'trebol_header_layout', 'trebol_header_layout_config');
if(!function_exists('trebol_header_layout_config')){
    function trebol_header_layout_config() {
        global  $post;
        $layout_header = get_theme_mod('trebol_header', 'full');
        if(is_page()){
            $layout_header = get_post_meta($post->ID, 'layout_header', true);
        }
        if($layout_header == 'global' || empty($layout_header)){
            $layout_header = get_theme_mod('trebol_header', 'full');
        }
        //Header Layout.
        get_template_part('templates/header/header', $layout_header);
        //Canvas Intro.
        get_template_part('templates/canvas-aside');
        //Mini Search
        get_template_part('templates/mini-search');
        //Header Mobile.
        get_template_part('templates/header/header', 'mobile');
    }
}

/* Keep Menu =========================================================================================================*/
if(!function_exists('trebol_keep_menu')){
    function trebol_keep_menu() {
        $configMenu = get_theme_mod('trebol_keep_menu',false);
        if(isset($configMenu) & $configMenu == '1'){
            $configMenu='header-fixed';
        }
        return $configMenu;
    }
}


/* Sub String Content =============================================================================================== */
if(!function_exists('trebol_content')) {
    function trebol_content($limit) {
        $excerpt = explode(' ', get_the_excerpt(), $limit);
        if (count($excerpt)>=$limit) {
            array_pop($excerpt);
            $excerpt = implode(" ",$excerpt).'...';
        } else {
            $excerpt = implode(" ",$excerpt);
        }
        $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
        return $excerpt;
    }
}
// Get Category  ==================================================================================================== /
if(!function_exists('trebol_category')) {
    function  trebol_category($separator)
    {
        $first_time = 1;
        foreach ((get_the_category()) as $category) {
            if ($first_time == 1) {?>
                <a href="<?php echo esc_url(get_category_link($category->term_id));?>"  title="<?php printf(esc_html__('View all posts in %s', 'trebol'), $category->name); ?>" >
                    <?php echo esc_attr($category->name);?>
                </a>
                <?php $first_time = 0; ?>
            <?php } else {
                echo esc_attr($separator) ?>
                <a href="<?php echo esc_url(get_category_link($category->term_id)); ?>" title="<?php printf(esc_html__('View all posts in %s', 'trebol'), $category->name) ?>" >
                    <?php  echo esc_attr($category->name); ?>
                </a>
            <?php }
        }
    }
}

/* Config Sidebar Blog ============================================================================================== */
add_action( 'single-sidebar-left', 'trebol_single_sidebar_left' );
function trebol_single_sidebar_left() {
    $single_sidebar=get_theme_mod( 'trebol_sidebar_single', 'right' );
    if ( $single_sidebar && $single_sidebar == 'left') { ?>
        <div id="archive-sidebar" class="sidebar sidebar-left hidden-xs hidden-sm col-xs-12 col-sm-12 col-md-3 col-lg-3 archive-sidebar">
            <?php get_sidebar('sidebar'); ?>
        </div>
    <?php }
}
add_action( 'single-sidebar-right', 'trebol_single_sidebar_right' );
function trebol_single_sidebar_right() {
    $single_sidebar=get_theme_mod( 'trebol_sidebar_single', 'right' );
    if ( $single_sidebar && $single_sidebar == 'right') {?>
        <div id="archive-sidebar" class="sidebar sidebar-right hidden-xs hidden-sm col-xs-12 col-sm-12 col-md-3 col-lg-3 archive-sidebar">
            <?php get_sidebar('sidebar'); ?>
        </div>
    <?php }
}
//content
add_action( 'single-content-before', 'trebol_single_content_before' );
function trebol_single_content_before() {
    $single_sidebar=get_theme_mod( 'trebol_sidebar_single', 'right' );
    if ( $single_sidebar && $single_sidebar == 'full') {?>
        <div class="main-content col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <?php }
    else{?>
        <div class="main-content col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <?php }
}
add_action( 'single-content-after', 'trebol_single_content_after' );
function trebol_single_content_after() {
    $single_sidebar=get_theme_mod( 'trebol_sidebar_single', 'right' );
    if ( $single_sidebar){?>
        </div>
    <?php }
}

/* Config Sidebar archive =========================================================================================== */
add_action( 'archive-sidebar-left', 'trebol_cat_sidebar_left' );
function trebol_cat_sidebar_left() {
    $cat_sidebar=get_theme_mod( 'trebol_sidebar_cat', 'full' );
    if(isset($_GET['layout'])){
        $cat_sidebar=$_GET['layout'];
    }
    if ( $cat_sidebar && $cat_sidebar == 'left') {?>
         <div id="archive-sidebar" class="sidebar sidebar-left hidden-xs hidden-sm col-xs-12 col-sm-12 col-md-3 col-lg-3 archive-sidebar">
            <?php get_sidebar('sidebar'); ?>
        </div>
    <?php }
}
add_action( 'archive-sidebar-right', 'trebol_cat_sidebar_right' );
function trebol_cat_sidebar_right() {
    $cat_sidebar=get_theme_mod( 'trebol_sidebar_cat', 'full' );
    if(isset($_GET['layout'])){
        $cat_sidebar=$_GET['layout'];
    }
    if ( $cat_sidebar && $cat_sidebar == 'right') {?>
         <div id="archive-sidebar" class="sidebar sidebar-right hidden-xs hidden-sm col-xs-12 col-sm-12 col-md-3 col-lg-3 archive-sidebar">
            <?php get_sidebar('sidebar'); ?>
        </div>
    <?php }
}
//content
add_action( 'archive-content-before', 'trebol_cat_content_before' );
function trebol_cat_content_before() {
    $cat_sidebar=get_theme_mod( 'trebol_sidebar_cat', 'full' );
    $layout_content      = get_theme_mod('trebol_cat_content_layout', 'list');
    if(isset($_GET['layout'])){
        $cat_sidebar=$_GET['layout'];
    }
    if(isset($_GET['content'])){
        $layout_content=$_GET['content'];
    }
    if ( $cat_sidebar && $cat_sidebar == 'full') {?>
        <div class="main-content col-md-12 col-lg-12 layout-<?php echo esc_attr($layout_content)?>">
    <?php }
    else{?>
        <div class="main-content col-xs-12 col-sm-12 col-md-9 col-lg-9 padding-content-<?php echo esc_attr($cat_sidebar)?> layout-<?php echo esc_attr($layout_content)?>">
    <?php }
}
add_action( 'archive-content-after', 'trebol_cat_content_after' );
function trebol_cat_content_after() {
    $cat_sidebar=get_theme_mod( 'trebol_sidebar_cat', 'full' );
    if ( $cat_sidebar){?>
        </div>
    <?php }
}

/* Category Title ===================================================================================================== */
    add_action( 'category-title', 'trebol_category_title' );
    function trebol_category_title() {
        $title  = get_theme_mod('trebol_post_title_heading',true);
        if ($title):?>
            <h1 class="page-title">
                <?php single_cat_title(); ?>
            </h1>
        <?php endif;
    }

/* Archive Title ===================================================================================================== */
add_action( 'archive-title', 'trebol_archive_title' );
function trebol_archive_title() { ?>
    <h1 class="page-title">
        <?php the_archive_title();?>
    </h1>
    <div class="description-page">
        <?php the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
    </div>
<?php }

/* Comment Form ===================================================================================================== */
function trebol_comment_form($arg,$class='btn-variant',$id='submit'){
    ob_start();
    comment_form($arg);
    $form = ob_get_clean();
    echo str_replace('id="submit"','id="'.$id.'"', $form);
}

function trebol_list_comments($comment, $args, $depth){
    $path = get_template_directory() . '/templates/list_comments.php';
    if( is_file($path) ) require ($path);
}

/* Move comment field to bottom ======================================================================================*/
function trebol_move_comment_field_to_bottom( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
}
add_filter( 'comment_form_fields', 'trebol_move_comment_field_to_bottom' );

?>
