<?php
/**
 * @package     trebol
 * @version     1.0
 * @author      Nanoagency
 * @link        http://www.nanoagency.co
 * @copyright   Copyright (c) 2018 Nanoagency
 * @license     GPL v2
 */

/* WooCommerce - Disable the default stylesheet WooCommerce ========================================================= */
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

// WooCommerce - remove add_to_cart from link_close ====================================================================
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
add_action('woocommerce_add_to_cart_item', 'woocommerce_template_loop_add_to_cart', 10);



// WooCommerce - cross_sell_display ====================================================================================
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
add_action( 'woo_cart_collaterals', 'woocommerce_cross_sell_display' );

// WooCommerce - remove result_count ===================================================================================
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count',20 );



// WooCommerce - Output the WooCommerce Breadcrumb =====================================================================
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
function trebol_woocommerce_breadcrumb( $args = array() ) {
    $args = wp_parse_args( $args, apply_filters( 'woocommerce_breadcrumb_defaults', array(
        'delimiter'   => '',
        'wrap_before' => '<div class = "breadcrumb"'.'>'. '<nav class="woocommerce-breadcrumb" ' . ( is_single() ? 'itemprop="breadcrumb"' : '' ) . '>',
        'wrap_after'  => '</nav></div>',
        'before'      => '',
        'after'       => '',
        'home'        => _x( 'Home', 'breadcrumb', 'trebol' )
    ) ) );

    $breadcrumbs = new WC_Breadcrumb();

    if ( $args['home'] ) {
        $breadcrumbs->add_crumb( $args['home'], apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );
    }

    $args['breadcrumb'] = $breadcrumbs->generate();

    wc_get_template( 'global/breadcrumb.php', $args );
}

// WooCommerce - Add Customize Popup Image to after Single Excerpt ================================================================
add_action('woocommerce_single_product_summary', 'trebol_popup_image_product', 21);
function trebol_popup_image_product() {
    $image_att  = get_post_meta( get_the_ID(), 'image_att_product', true );
    $name_att   = get_post_meta( get_the_ID(), 'name_att_product', true );
    $icon_att   = get_post_meta( get_the_ID(), 'icon_att_product', true );
    if($image_att){?>
         <button type="button" class="btn btn-sizeguide" data-toggle="modal" data-target="#trebol_popup_image">
            <img  class="icon-sizeguide" alt="icon-image" src="<?php echo esc_url(wp_get_attachment_url($icon_att)); ?>" alt="<?php echo esc_attr('icon_att'); ?>"/>
            <span><?php echo esc_html($name_att);?></span>
        </button>
        <div id="trebol_popup_image" class="trebol_popup_modal modal fade" tabindex="-1" role="dialog" aria-labelledby="trebol_popup_imageTitle" style="display: none;" aria-hidden="true">
          <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span class="icon ion-android-close"></span>
                </button>
              </div>
              <div class="modal-body">
                <img src="<?php echo esc_url(wp_get_attachment_url($image_att)); ?>" alt="<?php echo esc_attr('image_att'); ?>" />
              </div>
            </div>
        </div>

    <?php }
}

// **********************************************************************// 
// WooCommerce - Add list Attribute to before Single Excerpt 
// **********************************************************************// 
// 
if ( ! function_exists( 'trebol_list_att_product' ) ) :
    function trebol_list_att_product() {
        $list_atts  = get_post_meta( get_the_ID(), 'list_att_product', true );
        if($list_atts){?>
                <ul class="trebol_list_att list-unstyled">
                    <?php foreach ( $list_atts as $list_att) {?>
                        <?php if($list_att['name'] || $list_att['address']){ ?>
                            <li>
                                <label><?php echo esc_html($list_att['name']);?></label>
                                <span><?php echo esc_html($list_att['address']);?></span>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
        <?php }
    }
    // Uncomment the below
    add_action('woocommerce_single_product_summary', 'trebol_list_att_product', 19);
endif;

// WooCommerce -Sidebar Detail==========================================================================================
add_action( 'woo-sidebar-detail-left', 'trebol_woo_sidebar_left' );
function trebol_woo_sidebar_left() {
    $woo_sidebar=get_theme_mod( 'trebol_sidebar_woo_single', 'full' );
    if ( $woo_sidebar && $woo_sidebar == 'left') { ?>
        <div id="archive-sidebar" class="sidebar sidebar-left hidden-xs hidden-sm col-xs-12 col-sm-12 col-md-3 col-lg-3 detail-sidebar">
            <?php get_sidebar('shop'); ?>
        </div>
    <?php }
}

add_action( 'woo-sidebar-detail-right', 'trebol_woo_sidebar_right' );
function trebol_woo_sidebar_right() {
    $woo_sidebar=get_theme_mod( 'trebol_sidebar_woo_single', 'full' );
    if ( $woo_sidebar && $woo_sidebar == 'right') {?>
        <div id="archive-sidebar" class="sidebar sidebar-right hidden-xs hidden-sm col-xs-12 col-sm-12 col-md-3 col-lg-3 detail-sidebar">
            <?php get_sidebar('shop'); ?>
        </div>
    <?php }
}

//content
add_action( 'woo-content-detail-before', 'trebol_woo_content_before' );
function trebol_woo_content_before() {
    $woo_sidebar=get_theme_mod( 'trebol_sidebar_woo_single', 'full' );
    if ( $woo_sidebar && $woo_sidebar == 'full') {?>
        <div class="main-content">
    <?php }
    else{?>
        <div class="main-content col-xs-12 col-sm-12 col-md-9  col-lg-9 padding-content-<?php echo esc_attr($woo_sidebar)?>">
    <?php }
}
add_action( 'woo-content-detail-after', 'trebol_woo_content_after' );
function trebol_woo_content_after() {
    $woo_sidebar=get_theme_mod( 'trebol_sidebar_woo_single', 'full' );
    if ( $woo_sidebar){?>
        </div>
    <?php }
}
// WooCommerce -Sidebar Categories =====================================================================================
add_action( 'woo-sidebar-left', 'trebol_woo_sidebar_cat_left' );
function trebol_woo_sidebar_cat_left() {
    $woo_catLayout=get_theme_mod( 'trebol_filter_woo', 'left' );
    //get urlt
    if(isset($_GET['filter-woo'])){
        $woo_catLayout=$_GET['filter-woo'];
    }
    if ( $woo_catLayout && $woo_catLayout == 'left') { ?>
        <div id="archive-sidebar" class="sidebar shop-sidebar shop-filter sidebar-left col-xs-12 col-sm-12 col-md-4 col-lg-4 archive-sidebar">
            <div class="filter-header clearfix">
                    <span class="title-filter">
                        <?php  esc_html_e('Filter','trebol');?>
                    </span>
                    <button class="btn-mini-close pull-right"><i class="ti-close"></i></button>
            </div>
            <?php get_sidebar('shop'); ?>
        </div>
    <?php }
    if ( $woo_catLayout && $woo_catLayout == 'sidebar-left') {?>
        <div id="archive-sidebar" class="sidebar sidebar-left hidden-xs hidden-sm col-xs-12 col-sm-12 col-md-3 col-lg-3 archive-sidebar">
            <?php get_sidebar('shop'); ?>
        </div>
    <?php }
}

//content
add_action( 'woo-content-before', 'trebol_woo_content_cat_before' );
function trebol_woo_content_cat_before() {
        $woo_catLayout=get_theme_mod( 'trebol_filter_woo', 'left' );
        if(isset($_GET['filter-woo'])){
            $woo_catLayout=$_GET['filter-woo'];
        }
        if ( $woo_catLayout && $woo_catLayout == 'sidebar-left') { ?>
            <div class="main-content hidden-filters col-xs-12 col-sm-12 col-md-9 col-lg-9">
        <?php }else{?>
            <div class="main-content col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php }
}
add_action( 'woo-content-after', 'trebol_woo_content_cat_after' );
function trebol_woo_content_cat_after() {?>
        </div>
<?php }

/* WooCommerce - Show Filter ========================================================================================= */
add_action('woocommerce_before_shop_loop', 'trebol_nav_category', 17);
if ( ! function_exists( 'trebol_nav_category' ) ) {
    function trebol_nav_category() {
	        $woo_nav_check =   get_theme_mod( 'trebol_custom_nav_check', false);
	        $woo_catLayout  =   get_theme_mod( 'trebol_custom_nav', '' );
	        if(isset($_GET['navShop'])){
                    $woo_nav_check = $_GET['navShop'];
	        }
            if ($woo_nav_check) :?>
                <div class="woocommerce-nav-category clearfix">
                   <?php  echo wp_kses_post($woo_catLayout);?>
                </div>
	        <?php  elseif (has_nav_menu('shop_navigation')):?>
                <div class="woocommerce-nav-category clearfix">
                   <?php  wp_nav_menu( array(
                        'theme_location' => 'shop_navigation',
                        'menu_class'     => 'nav navbar-nav na-menu na-shop',
                        'container'      => '',
                    ) );?>
                </div>
            <?php endif; ?>

    <?php
    }
}

/* WooCommerce - Show Nav Filter ========================================================================================= */
add_action('woocommerce_before_shop_loop', 'trebol_nav_filters', 18);
if ( ! function_exists( 'trebol_nav_filters' ) ) {
    $trebol_layered_nav =get_theme_mod( 'trebol_woo_layered_nav', false );
    if(isset($_GET['layere-nav'])){
                $trebol_layered_nav=$_GET['layere-nav'];
    }
    function trebol_nav_filters() {?>
        <?php if ( isset($trebol_layered_nav)):?>
            <div class="woocommerce-nav-filters">
                <?php the_widget('WC_Widget_Layered_Nav_Filters', array('title' => esc_html__('Active Filters','trebol'))); ?>
            </div>
         <?php endif; ?>
    <?php
    }
}

/* WooCommerce - Show Filter ========================================================================================= */
add_action('woocommerce_before_shop_loop', 'trebol_btn_filter', 19);
if ( ! function_exists( 'trebol_btn_filter' ) ) {
    function trebol_btn_filter() {?>
        <?php $woo_catLayout=get_theme_mod( 'trebol_filter_woo', 'left' ); 
                if(isset($_GET['filter-woo'])){
                    $woo_catLayout=$_GET['filter-woo'];
                }
        ?>
        <div class="shop-btn-filter pull-right <?php echo esc_attr('btn-filter-'.$woo_catLayout)?>">
            <span class="btn-filter">                
                <?php  esc_html_e('Filters','trebol');?>
                <i class="ion ion-chevron-down" aria-hidden="true"></i>
            </span>
        </div>
    <?php
    }
}

// WooCommerce - Advanced Woo Search ===================================================================================
add_action( 'woocommerce_before_shop_loop', 'trebol_search',32 );
function trebol_search() {?>
    <div class="woo-search pull-right">
       <?php if ( function_exists( 'aws_get_search_form' ) ) { aws_get_search_form(); } ?>
    </div>
<?php
}

/* WooCommerce - Layout Filter ====================================================================================== */
add_action('woocommerce_before_shop_loop', 'trebol_filter_full', 33);
if ( ! function_exists( 'trebol_filter_full' ) ) {
    function trebol_filter_full() {?>
        <?php $woo_catLayout=get_theme_mod( 'trebol_filter_woo', 'left' ); 
                if(isset($_GET['filter-woo'])){
                    $woo_catLayout=$_GET['filter-woo'];
                }
        ?>
        <?php if ( $woo_catLayout && $woo_catLayout == 'full') { ?>
            <div id="filter-full" class="shop-filter filter-full col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php get_sidebar('shop'); ?>
            </div>
        <?php }?>
    <?php
    }
}
add_action('woocommerce_before_shop_loop', 'trebol_filter_down',31);
if ( ! function_exists( 'trebol_filter_down' ) ) {
    function trebol_filter_down() {?>
        <?php $woo_catLayout=get_theme_mod( 'trebol_filter_woo', 'left' ); 
               if(isset($_GET['filter-woo'])){
                    $woo_catLayout=$_GET['filter-woo'];
                } 
        ?>
        <?php if ( $woo_catLayout && $woo_catLayout == 'down') { ?>
            <div id="filter-down" class="shop-filter filter-down pull-right hidden-xs hidden-sm col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <?php get_sidebar('shop'); ?>
            </div>
        <?php }?>
    <?php
    }
}

// WooCommerce - Products per page ================================================================================== */
function trebol_get_products_per_page(){
    global $woocommerce;
    $trebol_get_products_per_page  = get_theme_mod('trebol_woo_product_per_page','16');
    $default = $trebol_get_products_per_page;
    $count = $default;
    return $count;
}
add_filter('loop_shop_per_page','trebol_get_products_per_page');

// WooCommerce - Next / Prev nav on Product Pages ======================================================================
function trebol_nav_product()
{
     // Get previous and next products.
    $in_same_term   = apply_filters( 'trebol_single_product_pagination_same_category', false );
    $excluded_terms = apply_filters( 'trebol_single_product_pagination_excluded_terms', '' );
    $taxonomy       = apply_filters( 'trebol_single_product_pagination_taxonomy', 'product_cat' );
    $previous_product = get_previous_post( $in_same_term, $excluded_terms, $taxonomy );
    $next_product     = get_next_post( $in_same_term, $excluded_terms, $taxonomy );
    if ( ! $previous_product && ! $next_product ) {
            return;
        }
    if ( $previous_product ) {
        $previous_product = wc_get_product( $previous_product->ID );
    }

    if ( $next_product ) {
        $next_product = wc_get_product( $next_product->ID );
    }

    if ( $previous_product && $previous_product->is_visible() ) :?>       
        <ul class="product-pagination previous-product-pagination list-unstyled">
            <li class="product-pagination-icon"><a title="<?php esc_html_e('prev','trebol')?>" href="<?php echo get_the_permalink($previous_product->get_id()); ?>" class="ion-ios-arrow-thin-left icons"></a></li>
            <li class="product-pagination-content">
                <?php 
                    previous_post_link( '%link', wp_kses_post( $previous_product->get_image() ) . '<span class="product-pagination-title">%title</span>', $in_same_term, $excluded_terms, $taxonomy );                    
                    echo '<div class="product-pagination-price">'. wp_kses_post( $previous_product->get_price_html()) .'</div>';
                ?>
            </li>            
        </ul><?php
    endif;
    if ( $next_product && $next_product->is_visible() ) :?>
        <ul class="product-pagination next-product-pagination list-unstyled">            
            <li class="product-pagination-icon"> <a title="<?php esc_html_e('next','trebol')?>" href="<?php echo get_the_permalink($next_product->get_id()); ?>" class="ion-ios-arrow-thin-right icons"></a> </li>
            <li class="product-pagination-content">
                <?php  
                    next_post_link( '%link', wp_kses_post( $next_product->get_image() ) . '<span class="product-pagination-title">%title</span>', $in_same_term, $excluded_terms, $taxonomy );
                    echo '<div class="product-pagination-price">'. wp_kses_post( $next_product->get_price_html()) .'</div>';
                ?>                
            </li>           
        </ul><?php
    endif;
}


// Product tabs: Change "Reviews" tab title ============================================================================
add_filter( 'woocommerce_product_reviews_tab_title', 'trebol_woocommerce_reviews_tab_title' );
function trebol_woocommerce_reviews_tab_title( $title ) {
    $title = strtr( $title, array(
        '(' => '<span>',
        ')' => '</span>'
    ) );

    return $title;
}

// Related Products: Change "related" ==================================================================================
add_filter( 'woocommerce_output_related_products_args', 'trebol_related_products_args' );
  function trebol_related_products_args( $args ) {
    $product_number                 = get_theme_mod('trebol_woo_number_related_products',4);
	$args['posts_per_page'] = $product_number; // 6 related products
	$args['columns'] = 4; // arranged in 4 columns
	return $args;
}
// Recently Viewed Products ============================================================================================
add_action( 'template_redirect', 'trebol_product_view', 20 );
function trebol_product_view() {
    if ( ! is_singular( 'product' ) ) {
        return;
    }
    global $post;
    if ( empty( $_COOKIE['woocommerce_recently_viewed'] ) )
        $viewed_products = array();
    else
        $viewed_products = (array) explode( '|', $_COOKIE['woocommerce_recently_viewed'] );
    if ( ! in_array( $post->ID, $viewed_products ) ) {
        $viewed_products[] = $post->ID;
    }
    if ( sizeof( $viewed_products ) > 15 ) {
        array_shift( $viewed_products );
    }
    // Store for session only
    wc_setcookie( 'woocommerce_recently_viewed', implode( '|', $viewed_products ) );
}
add_action("woocommerce_recently_viewed_products", "trebol_woocommerce_recently_viewed_products");
function trebol_woocommerce_recently_viewed_products( $atts, $content = null ) {
     global $post;
     $viewed_products = ! empty( $_COOKIE['woocommerce_recently_viewed'] ) ? (array) explode( '|', $_COOKIE['woocommerce_recently_viewed'] ) : array();
     $viewed_products = array_filter( array_map( 'absint', $viewed_products ) );
     if ( empty( $viewed_products ) )
         return __( 'You have not viewed any product yet!', 'rc_wc_rvp' );

     $query_args = array(
         'posts_per_page' => 4,
         'no_found_rows'  => 1,
         'post_status'    => 'publish',
         'post_type'      => 'product',
         'post__in'       => $viewed_products,
         'orderby'        => 'rand'
     );
     $query_args['meta_query'] = array();
     $query_args['meta_query'][] = WC()->query->stock_status_meta_query();
     $r = new WP_Query($query_args);
     if (empty($r)) {
         return __( 'You have not viewed any product yet!', 'rc_wc_rvp' );
     }?>
     <?php while ( $r->have_posts() ) : $r->the_post();
         wc_get_template_part( 'content', 'product-related' ); ?>
     <?php endwhile; ?>
     <?php wp_reset_postdata();
}

/* WooCommerce - Ajax add to cart =================================================================================== */
add_action('woocommerce_ajax_added_to_cart', 'trebol_ajax_added_to_cart');
function trebol_ajax_added_to_cart() {
    add_filter( 'add_to_cart_fragments', 'trebol_header_add_to_cart_fragment' );
    function trebol_header_add_to_cart_fragment( $fragments ) {
        ob_start();
        trebol_cartbox();
        $fragments['.na-cart'] = ob_get_clean();
        return $fragments;
    }
}
if(!function_exists('trebol_cartbox')){
    function trebol_cartbox(){
        global $woocommerce;
        $cart_image = get_theme_mod('trebol_cart_image', '');
        ?>
        <div class="na-cart">
            <div  class="mini-cart btn-header clearfix" role="contentinfo">
                <span class="icon-cart text-items">
                    <?php if($cart_image){ ?>
                        <img class="cart_image" src="<?php echo esc_url($cart_image); ?>" alt="cart-image">
                    <?php } else{?>
                        <i class="icon-handbag icons"></i>
                    <?php }?>
                    <?php echo sprintf(_n(' <span class="mini-cart-items">%d</span> ', '<span class="mini-cart-items">%d</span>', $woocommerce->cart->cart_contents_count, 'trebol'), $woocommerce->cart->cart_contents_count); ?>
                </span>
            </div>
            <div class="cart-box">
                <?php woocommerce_mini_cart(); ?>
            </div>
        </div>
    <?php }
}

/* WooCommerce - Product: Get sale percentage ======================================================================= */
function trebol_product_get_sale_percent( $product ) {
    if ( $product->is_type( 'variable' ) ) {
        // Get product variation prices (regular and sale)
        $product_variation_prices = $product->get_variation_prices();
        $highest_sale_percent = 0;
        foreach( $product_variation_prices['regular_price'] as $key => $regular_price ) {
            // Get sale price for current variation
            $sale_price = $product_variation_prices['sale_price'][$key];

            // Is product variation on sale?
            if ( $sale_price < $regular_price ) {
                $sale_percent = round( ( ( $regular_price - $sale_price ) / $regular_price ) * 100 );

                // Is current sale percent highest?
                if ( $sale_percent > $highest_sale_percent ) {
                    $highest_sale_percent = $sale_percent;
                }
            }
        }
        return $highest_sale_percent;
    } else {
        $sale_percent = 0;
        if ( intval( $product->get_regular_price() ) > 0 ) {
            $sale_percent = round( ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100 );
        }
        return $sale_percent;
    }
}

/* WooCommerce - Register Size Thumbnail ============================================================================ */
remove_action( 'woocommerce_before_subcategory_title', 'woocommerce_subcategory_thumbnail', 10 );

add_action( 'trebol-category-thumb-large', 'trebol_subcategory_thumbnail_large' );
if ( ! function_exists( 'trebol_subcategory_thumbnail_large' ) ) {
	function trebol_subcategory_thumbnail_large( $category ) {
		$small_thumbnail_size = apply_filters( 'subcategory_archive_thumbnail_size', 'trebol-category-thumb-large' );
        $placeholder_image    = get_template_directory_uri(). '/assets/images/layzyload-category-thumb-large.jpg';
		$dimensions           = wc_get_image_size( $small_thumbnail_size );
		$thumbnail_id         = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );

		if ( $thumbnail_id ) {
			$image        = wp_get_attachment_image_src( $thumbnail_id, $small_thumbnail_size );
			$image        = $image[0];
			$image_srcset = function_exists( 'wp_get_attachment_image_srcset' ) ? wp_get_attachment_image_srcset( $thumbnail_id, $small_thumbnail_size ) : false;
            $image_sizes  = function_exists( 'wp_get_attachment_image_sizes' ) ? wp_get_attachment_image_sizes( $thumbnail_id, $small_thumbnail_size ) : false;
            $thumb_size   =wp_get_attachment_image_src($thumbnail_id,'trebol-category-thumb-large');
		} else {
			$image        = $placeholder_image;
			$image_srcset = false;
			$image_sizes  = false;
		}

		if ( $image ) {
			$image = str_replace( ' ', '%20', $image );

			// Add responsive image markup if available.
			if ( $image_srcset && $image_sizes ) {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" srcset="' . esc_attr( $image_srcset ) . '" sizes="' . esc_attr( $image_sizes ) . '" />';
			} else {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" />';
			}
		}
	}
}

add_action( 'trebol-category-thumb-wide', 'trebol_subcategory_thumbnail_wide' );
if ( ! function_exists( 'trebol_subcategory_thumbnail_wide' ) ) {
	function trebol_subcategory_thumbnail_wide( $category ) {
		$small_thumbnail_size = apply_filters( 'subcategory_archive_thumbnail_size', array('580','310'),true );
        $placeholder_image    = get_template_directory_uri(). '/assets/images/layzyload-category-thumb-wide.jpg';
		$dimensions           = wc_get_image_size( $small_thumbnail_size );
		$thumbnail_id         = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );

		if ( $thumbnail_id ) {
			$image        = wp_get_attachment_image_src( $thumbnail_id, array('580','310'), true );
			$image        = $image[0];
			$image_srcset = function_exists( 'wp_get_attachment_image_srcset' ) ? wp_get_attachment_image_srcset( $thumbnail_id,$small_thumbnail_size ) : false;
			$image_sizes  = function_exists( 'wp_get_attachment_image_sizes' ) ? wp_get_attachment_image_sizes( $thumbnail_id, $small_thumbnail_size) : false;
			$thumb_size   = wp_get_attachment_image_src($thumbnail_id,array('580','310'),true);


		} else {
			$image        = $placeholder_image;
			$image_srcset = false;
			$image_sizes  = false;
		}
        
		if ( $image ) {
			$image = str_replace( ' ', '%20', $image );

			// Add responsive image markup if available.
			if ( $image_srcset && $image_sizes ) {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" srcset="' . esc_attr( $image_srcset ) . '" sizes="' . esc_attr( $image_sizes ) . '" />';
			} else {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" sizes="' . esc_attr( $image_sizes ) . '" />';
			}
		}
	}
}


add_action( 'trebol-category-thumb', 'trebol_subcategory_thumbnail' );
if ( ! function_exists( 'trebol_subcategory_thumbnail' ) ) {
	function trebol_subcategory_thumbnail( $category ) {
		$small_thumbnail_size = apply_filters( 'subcategory_archive_thumbnail_size', 'trebol-category-thumb' );
        $placeholder_image    = get_template_directory_uri(). '/assets/images/layzyload-category-thumb.jpg';
		$dimensions           = wc_get_image_size( $small_thumbnail_size );
		$thumbnail_id         = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );

		if ( $thumbnail_id ) {
			$image        = wp_get_attachment_image_src( $thumbnail_id, $small_thumbnail_size );
			$image        = $image[0];
			$image_srcset = function_exists( 'wp_get_attachment_image_srcset' ) ? wp_get_attachment_image_srcset( $thumbnail_id, $small_thumbnail_size ) : false;
			$image_sizes  = function_exists( 'wp_get_attachment_image_sizes' ) ? wp_get_attachment_image_sizes( $thumbnail_id, $small_thumbnail_size ) : false;
			$thumb_size   =wp_get_attachment_image_src($thumbnail_id,'trebol-category-thumb');
		} else {
			$image        = $placeholder_image;
			$image_srcset = false;
			$image_sizes  = false;
		}
		if ( $image ) {
			$image = str_replace( ' ', '%20', $image );

			// Add responsive image markup if available.
			if ( $image_srcset && $image_sizes ) {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" srcset="' . esc_attr( $image_srcset ) . '" sizes="' . esc_attr( $image_sizes ) . '" />';
			} else {
				echo '<img src="' . esc_url( $image ) . '" alt="' . esc_attr( $category->name ) . '" />';
			}
		}
	}
}

/* WooCommerce - Register subcategory_title ========================================================================= */

remove_action( 'woocommerce_shop_loop_subcategory_title', 'woocommerce_template_loop_category_title', 10 );
add_action( 'woocommerce_shop_loop_subcategory_title', 'trebol_template_loop_category_title', 10 );

if ( ! function_exists( 'trebol_template_loop_category_title' ) ) {

    /**
     * Show the subcategory title in the product loop.
     *
     * @param object $category Category object.
     */
    function trebol_template_loop_category_title( $category ) {
        ?>
        <h2 class="woocommerce-loop-category__title">
            <a href="<?php echo esc_url(get_term_link($category)); ?>">
                <span><?php echo esc_html( $category->name ); ?></span>
            </a>
            <?php
                if ( $category->count > 0 ) {
                    echo apply_filters( 'woocommerce_subcategory_count_html', ' <mark class="count">' . esc_html( $category->count ) . ' '.esc_html__('items','trebol').'</mark>', $category ); // WPCS: XSS ok.
                }
            ?>
            <span class="btn-shop"><?php  esc_html_e('Shop Now','trebol');?></span>
        </h2>
    <?php }
}

/**
 * Product loop start.
 */

remove_filter( 'woocommerce_product_loop_start', 'woocommerce_maybe_show_product_subcategories' );

add_filter( 'trebol_subcategories_loop_start', 'woocommerce_maybe_show_product_subcategories' );

if ( ! function_exists( 'trebol_subcategories_loop_start' ) ) {

	/**
	 * Output the start of a product loop. By default this is a UL.
	 *
	 * @param bool $echo Should echo?.
	 * @return string
	 */
	function trebol_subcategories_loop_start( $echo = true ) {
        
		ob_start();
		wc_set_loop_prop( 'loop', 0 );

		wc_get_template( 'loop/loop-start-subcate.php' );

        if(isset($_GET['shop-type']) && ($_GET['shop-type']=='both')){
            $args = array(
            'hide_empty' => 1,
            'pad_counts' => true,
            'child_of'   => 0,
            'parent'      => 0,
            );
            $product_categories = get_terms( 'product_cat', $args);
            if ( $product_categories ) {
                foreach ( $product_categories as $category ) {
                    wc_get_template( 'content-product_cat.php', array(
                        'category' => $category,
                    ) );
                }
            }
        }

        $loop_start = apply_filters( 'trebol_subcategories_loop_start', ob_get_clean() );

        if ( $echo ) {
            echo $loop_start; // WPCS: XSS ok.
        } else {
            return $loop_start;
        }
	}
}

/**
 * Product layout.
 */
$detailLayouts = get_theme_mod('trebol_detail_layouts', 'vertical');
if(isset($_GET['detail-layouts'])){
        $detailLayouts=$_GET['detail-layouts'];
}
// Vertical layout Default
function trebol_variation_image_size_vertical( $child_id, $instance, $variation ) {
  $attachment_id = get_post_thumbnail_id( $variation->get_id() );
  $attachment    = wp_get_attachment_image_src( $attachment_id, 'trebol-product-vertical' );
  $image_src     = $attachment ? current( $attachment ) : '';
  $child_id['image']['src'] = $image_src;
  $child_id['image']['srcset'] = $image_src;
  $child_id['image']['src_w'] = '767';
  $child_id['image']['src_h'] = '905';
  return $child_id;
}
if($detailLayouts == 'vertical'){
    add_filter( 'woocommerce_available_variation', 'trebol_variation_image_size_vertical', 10, 3 );
}

// Horizontal layout
add_action( 'trebol_before_single_product_summary_horizontal', 'woocommerce_show_product_sale_flash', 10 );
add_action( 'trebol_before_single_product_summary_horizontal', 'trebol_show_product_images_horizontal', 20 );
add_action( 'trebol_product_thumbnails_horizontal', 'trebol_show_product_thumbnails_horizontal', 20 );

if ( ! function_exists( 'trebol_show_product_images_horizontal' ) ) {

	/**
	 * Output the product image before the single product summary.
	 */
	function trebol_show_product_images_horizontal() {
		wc_get_template( 'single-product/layouts/horizontal/product-image.php' );
	}
}
if ( ! function_exists( 'trebol_show_product_thumbnails_horizontal' ) ) {

	/**
	 * Output the product thumbnails.
	 */
	function trebol_show_product_thumbnails_horizontal() {
		wc_get_template( 'single-product/layouts/horizontal/product-thumbnails.php' );
	}
}

function trebol_variation_image_size_horizontal( $child_id, $instance, $variation ) {
  $attachment_id = get_post_thumbnail_id( $variation->get_id() );
  $attachment    = wp_get_attachment_image_src( $attachment_id, 'trebol-product-horizontal' );
  $image_src     = $attachment ? current( $attachment ) : '';
  $child_id['image']['src'] = $image_src;
  $child_id['image']['srcset'] = $image_src;
  $child_id['image']['src_w'] = '570';
  $child_id['image']['src_h'] = '673';
  return $child_id;
}
if($detailLayouts == 'horizontal'){
    add_filter( 'woocommerce_available_variation', 'trebol_variation_image_size_horizontal', 10, 3 );
}


// background layout
add_action( 'trebol_before_single_product_summary_background', 'woocommerce_show_product_sale_flash', 10 );
add_action( 'trebol_before_single_product_summary_background', 'trebol_show_product_images_background', 20 );
add_action( 'trebol_product_thumbnails_background', 'trebol_show_product_thumbnails_background', 20 );

if ( ! function_exists( 'trebol_show_product_images_background' ) ) {

    /**
     * Output the product image before the single product summary.
     */
    function trebol_show_product_images_background() {
        wc_get_template( 'single-product/layouts/background/product-image.php' );
    }
}
if ( ! function_exists( 'trebol_show_product_thumbnails_background' ) ) {

    /**
     * Output the product thumbnails.
     */
    function trebol_show_product_thumbnails_background() {
        wc_get_template( 'single-product/layouts/background/product-thumbnails.php' );
    }
}

// Grid layout
add_action( 'trebol_before_single_product_summary_grid', 'woocommerce_show_product_sale_flash', 10 );
add_action( 'trebol_before_single_product_summary_grid', 'trebol_show_product_images_grid', 20 );
if ( ! function_exists( 'trebol_show_product_images_grid' ) ) {

	/**
	 * Output the product image before the single product summary.
	 */
	function trebol_show_product_images_grid() {
		wc_get_template( 'single-product/layouts/grid/product-image.php' );
	}
}
function trebol_variation_image_size_grid( $child_id, $instance, $variation ) {
  $attachment_id = get_post_thumbnail_id( $variation->get_id() );
  $attachment    = wp_get_attachment_image_src( $attachment_id, 'trebol-product-grid' );
  $image_src     = $attachment ? current( $attachment ) : '';
  $child_id['image']['src'] = $image_src;
  $child_id['image']['srcset'] = $image_src;
  $child_id['image']['src_w'] = '320';
  $child_id['image']['src_h'] = '320';
  return $child_id;
}
if($detailLayouts == 'grid'){
    add_filter( 'woocommerce_available_variation', 'trebol_variation_image_size_grid', 10, 3 );
}

// Carousel layout
add_action( 'trebol_before_single_product_summary_carousel', 'woocommerce_show_product_sale_flash', 10 );
add_action( 'trebol_before_single_product_summary_carousel', 'trebol_show_product_images_carousel', 20 );

if ( ! function_exists( 'trebol_show_product_images_carousel' ) ) {

	/**
	 * Output the product image before the single product summary.
	 */
	function trebol_show_product_images_carousel() {
		wc_get_template( 'single-product/layouts/carousel/product-image.php' );
	}
}

// Carousel  Left layout
add_action( 'trebol_before_single_product_summary_carousel_left', 'woocommerce_show_product_sale_flash', 10 );
add_action( 'trebol_before_single_product_summary_carousel_left', 'trebol_show_product_images_carousel_left', 20 );

if ( ! function_exists( 'trebol_show_product_images_carousel_left' ) ) {
	/**
	 * Output the product image before the single product summary.
	 */
	function trebol_show_product_images_carousel_left() {
		wc_get_template( 'single-product/layouts/carousel-left/product-image.php' );
	}
}

//Layout  Sticky
add_action( 'trebol_before_single_product_summary_carousel_sticky', 'woocommerce_show_product_sale_flash', 10 );
add_action( 'trebol_before_single_product_summary_carousel_sticky', 'trebol_show_product_images_sticky', 20 );

if ( ! function_exists( 'trebol_show_product_images_sticky' ) ) {
    /**
     * Output the product image before the single product summary.
     */
    function trebol_show_product_images_sticky() {
        wc_get_template( 'single-product/layouts/sticky/product-image.php' );
    }
}
// WooCommerce - Layout SHOP ========================================================================================= /
//galerry lg
add_action( 'trebol_galerry_before_shop_loop_item_title', 'trebol_galerry_template_loop_product_thumbnail', 10 );
add_action( 'trebol_galerry_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );

if ( ! function_exists( 'trebol_galerry_template_loop_product_thumbnail' ) ) {
    function trebol_galerry_template_loop_product_thumbnail() {
        echo trebol_galerry_get_product_thumbnail();
    }
}

function trebol_galerry_get_product_thumbnail() {
    $image_src                      = wp_get_attachment_image_src(get_post_thumbnail_id(), 'trebol-galerry-thumbnail-size');
    $placeholder_image              = get_template_directory_uri(). '/assets/images/layzyload-product-galerry.jpg';
    $placeholder_fallback = wc_placeholder_img_src();
    $alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
    $size = $image_src;
    $woo_width = $size[1];
    $woo_height = $size[2];

    if (!empty($image_src)) {
        echo '<img src="' . $placeholder_image . '" data-lazy="' . $image_src[0] . '" data-src="' . $image_src[0] . '" width="' . $woo_width . '" height="' . $woo_height . '" class="attachment-shop_catalog wp-post-image lazy" alt="' . esc_attr($alt) . '">';
    } else {
        echo '<img src="' . $placeholder_fallback . '" data-lazy="' . $image_src[0] . '" data-src="' . $image_src[0] . '" width="' . $woo_width . '" height="' . $woo_height . '" class="attachment-shop_catalog wp-post-image lazy" alt="' . esc_attr($alt) . '">';
    }
}

//grid
if (!function_exists('woocommerce_template_loop_product_thumbnail')) {
    remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
    add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);

    function woocommerce_template_loop_product_thumbnail() {
        $image_src = wp_get_attachment_image_src(get_post_thumbnail_id(), 'shop_catalog');
        $placeholder_image              = get_template_directory_uri(). '/assets/images/layzyload-product-grid.jpg';

        $placeholder_fallback = wc_placeholder_img_src();
        $alt = get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true);
        $size = wc_get_image_size('shop_catalog');
        $woo_width = $size['width'];
        $woo_height = $size['height'];

        if (!empty($image_src)) {
            echo '<img src="' . $placeholder_image . '" data-lazy="' . $image_src[0] . '" data-src="' . $image_src[0] . '" width="' . $woo_width . '" height="' . $woo_height . '" class="attachment-shop_catalog wp-post-image lazy" alt="' . esc_attr($alt) . '">';
        } else {
            echo '<img src="' . $placeholder_fallback . '" data-lazy="' . $image_src[0] . '" data-src="' . $image_src[0] . '" width="' . $woo_width . '" height="' . $woo_height . '" class="attachment-shop_catalog wp-post-image lazy" alt="' . esc_attr($alt) . '">';
        }
    }
}
?>