<?php
 /* *
  * widgets contact info
  **/
  class trebol_contact_info extends WP_Widget{

      /*function construct*/
      public function __construct() {
          parent::__construct(
            'contact_info',__('+NA: Contact info','trebol'),
             array('description'=>__('Display Contact info', 'trebol'))
          );
      }
      /**
       * font-end widgets
      */
      public function widget($args, $instance) {
          extract($args);
          $title = apply_filters('widget_title', $instance['title']);

          echo ent2ncr($args['before_widget']);

          if($title) {
              echo ent2ncr($args['before_title']) . esc_html($title) . ent2ncr($args['after_title']);
          }

      ?>

          <?php if($instance['description']): ?>
              <p class="description">
                  <span><?php echo esc_attr($instance['description']); ?></span>
              </p>
          <?php endif; ?>

          <ul class="contact-info list-unstyled">
                <?php  if($instance['address']): ?>
                <li class="address">
                    <b><?php  echo esc_html('Address: ','trebol');  ?></b>
                    <span><?php  echo esc_attr($instance['address']);  ?></span>
                </li>
                <?php  endif; ?>

                <?php if($instance['mobile']): ?>
                  <li class="mobile">
                      <b><?php  echo esc_html('Mobile: ','trebol');  ?></b>
                      <span><?php   echo esc_attr($instance['mobile']);  ?></span>
                  </li>

                <?php endif; ?>
                <?php if($instance['phone']): ?>
                  <li class="phone">
                      <b><?php  echo esc_html('Phone: ','trebol');  ?></b>
                      <span><?php   echo esc_attr($instance['phone']);  ?></span>
                  </li>

                <?php endif; ?>

                <?php if($instance['skype']): ?>
                    <li class="skype">
                        <b><?php  echo esc_html('Skype: ','trebol');  ?></b>
                        <a href="skype:<?php echo esc_attr($instance['skype']);?>?chat" ><span><?php echo esc_attr($instance['skype']); ?></span></a>
                    </li>
                <?php endif; ?>

                <?php if($instance['email']): ?>
                    <li class="email">
                        <b><?php  echo esc_html('Email: ','trebol');  ?></b>
                        <a href="mailto:<?php echo esc_attr($instance['email']);?>" ><span><?php echo esc_attr($instance['email']); ?></span></a>
                    </li>
                <?php endif; ?>
          </ul>

      <?php
          echo ent2ncr($args['after_widget']);
      }

      /**
       * Back-end widgets form
      */
      public function form($instance){
          $instance =   wp_parse_args($instance,array(
              'title'       =>  'Contact info',
              'address'     =>  '',
              'phone'       =>  '',
              'mobile'      =>  '',
              'skype'       =>  '',
              'email'       =>  '',
              'description' =>'',
          ));
          ?>
          <p>
              <label for=<?php echo esc_attr($this->get_field_id('title')); ?>><?php echo esc_html_e('Title:','trebol') ; ?></label>
              <input type="text" id="<?php echo esc_attr($this->get_field_id('title')); ?>" class="widefat" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
          </p>
          <p>
              <label for="<?php echo esc_attr($this->get_field_id('address')); ?>"><?php echo esc_html_e('Address:','trebol'); ?></label>
              <input type="text" id="<?php echo esc_attr($this->get_field_id('address')); ?>" class="widefat" name="<?php echo esc_attr($this->get_field_name('address')); ?>" value="<?php echo esc_attr($instance['address']); ?>" />
          </p>
          <p>
              <label for="<?php echo esc_attr($this->get_field_id('phone')); ?>"><?php echo esc_html_e( 'Phone:', 'trebol' ); ?></label>
              <input type="text" id="<?php echo esc_attr($this->get_field_id('phone')); ?>" class="widefat" name="<?php echo esc_attr($this->get_field_name('phone')); ?>" value="<?php echo esc_attr($instance['phone']); ?>" />
          </p>
          <p>
              <label for="<?php echo esc_attr($this->get_field_id('mobile')); ?>"><?php echo esc_html_e( 'Mobile:', 'trebol' ); ?></label>
              <input type="text" id="<?php echo esc_attr($this->get_field_id('mobile')); ?>" class="widefat" name="<?php echo esc_attr($this->get_field_name('mobile')); ?>" value="<?php echo esc_attr($instance['mobile']); ?>" />
          </p>
          <p>
              <label for="<?php echo esc_attr($this->get_field_id('skype')); ?>"><?php echo esc_html_e('Skype:', 'trebol'); ?></label>
              <input type="text" id="<?php echo esc_attr($this->get_field_id('skype')); ?>" name="<?php echo esc_attr($this->get_field_name('skype')); ?>" class="widefat" value="<?php echo esc_attr($instance['skype']); ?>" />
          </p>
          <p>
              <label for="<?php echo esc_attr($this->get_field_id('email')); ?>"><?php echo esc_html_e('Email:', 'trebol'); ?></label>
              <input type="text" id="<?php echo esc_attr($this->get_field_id('email')); ?>" name="<?php echo esc_attr($this->get_field_name('email')); ?>" class="widefat" value="<?php echo esc_attr($instance['email']); ?>" />
          </p>

          <!-- description -->
          <p>
              <label for="<?php echo esc_attr($this->get_field_id( 'description' )); ?>"><?php echo esc_html_e('About me text::', 'trebol'); ?></label>
              <textarea id="<?php echo esc_attr($this->get_field_id( 'description')); ?>" name="<?php echo esc_attr($this->get_field_name( 'description' )); ?>" style="width:95%;" rows="6"><?php echo esc_attr($instance['description']); ?></textarea>
          </p>


      <?php
      }

      /**
      * function update widget
      */
      public function update( $new_instance, $old_instance ) {
          $instance = $old_instance;
          $instance['title'] = $new_instance['title'];
          $instance['address'] = $new_instance['address'];
          $instance['phone']    =   $new_instance['phone'];
          $instance['mobile'] = $new_instance['mobile'];
          $instance['skype'] = $new_instance['skype'];
          $instance['email']    =   $new_instance['email'];
          $instance['description']    =   $new_instance['description'];
          return $instance;
      }
  }
  function trebol_contact_info(){
      register_widget('trebol_contact_info');
  }
  add_action('widgets_init','trebol_contact_info');
?>