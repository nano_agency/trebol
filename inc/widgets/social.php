<?php
if (!class_exists('trebol_social')) {
    class trebol_social extends WP_Widget
    {
        public $socials = array(
            'ion-facebook' => array(
                'title' => 'facebook',
                'name' => 'facebook_username',
                'link' => '*',
                'icon'=>'ion-social-facebook',
            ),
            'ion-google-plus' => array(
                'title' => 'googleplus',
                'name' => 'googleplus_username',
                'link' => '*',
                'icon'=>'ion-social-googleplus',
            ),
            'ion-twitter' => array(
                'title' => 'twitter',
                'name' => 'twitter_username',
                'link' => '*',
                'icon'=>'ion-social-twitter',
            ),
            'ion-instagram' => array(
                'title' => 'instagram',
                'name' => 'instagram_username',
                'link' => '*',
                'icon'=>'ion-social-instagram',
            ),
            'ion-pinterest' => array(
                'title' => 'pinterest',
                'name' => 'pinterest_username',
                'link' => '*',
                'icon'=>'ion-social-pinterest',
            ),
            'ion-skype' => array(
                'title' => 'skype',
                'name' => 'skype_username',
                'link' => '*',
                'icon'=>'ion-social-skype',
            ),
            'ion-vimeo' => array(
                'title' => 'vimeo',
                'name' => 'vimeo_username',
                'link' => '*',
                'icon'=>'ion-social-vimeo',
            ),
            'ion-youtube' => array(
                'title' => 'youtube',
                'name' => 'youtube_username',
                'link' => '*',
                'icon'=>'ion-social-youtube',
            ),
            'ion-dribbble' => array(
                'title' => 'dribbble',
                'name' => 'dribbble_username',
                'link' => '*',
                'icon'=>'ion-social-dribbble',
            ),
            'ion-linkedin' => array(
                'title' => 'linkedin',
                'name' => 'linkedin_username',
                'link' => '*',
                'icon'=>'ion-social-linkedin',
            ),
            'ion-rss' => array(
                'title' => 'rss',
                'name' => 'rss_username',
                'link' => '*',
                'icon'=>'ion-social-rss',
            )
        );

        public function __construct() 
        {
            $widget_ops = array('classname' => 'trebol_social', 'description' => esc_html__('Displays your social profile.', 'trebol'));

            parent::__construct(false, esc_html__('+NA: Social', 'trebol'), $widget_ops);
        }

        function widget($args, $instance)
        {
            extract($args);
            $title = apply_filters('widget_title', $instance['title']);
            echo htmlspecialchars_decode(esc_html($before_widget));
            if ($title) {
                echo htmlspecialchars_decode(esc_html($before_title . $title . $after_title));
            }
            echo '<div class="share-links clearfix">';
            echo "<ul class='social-icons list-unstyled list-inline'>";
            foreach ($this->socials as $key => $social) {
                if (!empty($instance[$social['name']])) {
                    echo "<li class='social-item'>";
                    echo '<a href="' . str_replace('*', esc_attr($instance[$social['name']]), $social['link']) . '" target="_blank" title="' . esc_attr($key) . '" class="' . esc_attr($key) . '"><i class="icons ' . esc_attr( $social['icon']) . '"></i></a>';
                    echo "</li>";
                }
            }echo "</ul>";
            echo '</div>';
            echo htmlspecialchars_decode(esc_html($after_widget));
        }

        function update($new_instance, $old_instance)
        {
            $instance = $old_instance;
            $instance = $new_instance;
            /* Strip tags (if needed) and update the widget settings. */
            $instance['title'] = strip_tags($new_instance['title']);
            return $instance;
        }

        function form($instance)
        {
            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>">Title:</label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" type="text"
                       name="<?php echo esc_attr($this->get_field_name('title')); ?>"
                       value="<?php echo isset($instance['title']) ? esc_attr($instance['title']) : ''; ?>"/>
            </p> <?php
            foreach ($this->socials as $key => $social) {
                ?>
                <p>
                <label for="<?php echo esc_attr($this->get_field_id($social['name'])); ?>"><?php echo esc_html($key); ?>
                    :</label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id($social['name'])); ?>" type="text"
                       name="<?php echo esc_attr($this->get_field_name($social['name'])); ?>"
                       value="<?php echo isset($instance[$social['name']]) ? esc_attr($instance[$social['name']]) : ''; ?>"/>
                </p><?php
            }
        }
    }
}

add_action('widgets_init', 'trebol_social_widgets');

function trebol_social_widgets()
{
    register_widget('trebol_social');
}
