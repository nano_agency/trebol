<?php
/**
 * The template for displaying pages
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */
get_header();
$trebol_title     = get_post_meta(get_the_ID(), 'trebol_show_title',true);
?>

<?php if( ($trebol_title=== '1') || ($trebol_title=='')){?>
    <section class="wrap-breadcrumb">
        <div class="container">
            <h1 class="page-title">
                <?php the_title(); ?>
            </h1>
            <?php trebol_woocommerce_breadcrumb(); ?>
        </div>
    </section>
<?php }?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="container">                
            <?php
                while ( have_posts() ) : the_post();?>
                    <?php get_template_part( 'content', 'page' );
                    if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif;
                endwhile;
            ?>
        </div>
    </main>
</div>

<?php get_footer(); ?>