<?php
/**
 * The template for displaying the footer
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */
?>
<footer class="na-footer">
    <div class="footer-bottom footer-hidden clearfix">
        <div class="container">
            <div class="container-inner">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 footer-bottom-left">
                        <div class="footer-bottom-inner">
                            <?php if(get_theme_mod('trebol_copyright_text')) {?>
                                <span><?php echo  wp_kses_post(get_theme_mod('trebol_copyright_text'));?></span>
                            <?php }else{?>
                                <?php echo '<span>'.esc_html('&copy; Copyrights ','trebol').' '.date("Y").'<a href="'.esc_url('http://trebol.nanoagency.co').'">'.esc_html('  trebol theme. ','trebol').'</a>'; ?>
                            <?php }?>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 footer-bottom-right">
                        <div class="footer-bottom-inner">
                            <?php if(is_active_sidebar( 'footer-bottom-right' )){
                                dynamic_sidebar('footer-bottom-right');
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</footer>