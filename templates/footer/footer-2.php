<?php
/**
 * The template for displaying the footer
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */
?>
<footer id="na-footer" class="na-footer footer-2">
    <!--    Footer bottom-->
    <div class="footer-bottom clearfix">
        <div class="container">
            <div class="container-inner">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 footer-bottom-left">
                        <div class="footer-bottom-inner">
                            <?php if(is_active_sidebar( 'footer2-column1' )){ ?>
                                 <?php  dynamic_sidebar('footer2-column1');?>
                            <?php }?>
                            <?php echo '<span>'.esc_html('&copy; Copyrights ','trebol').' '.date("Y").'<a href="'.esc_url('http://trebol.nanoagency.co').'">'.esc_html('  trebol theme. ','trebol').'</a>'; ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 footer-bottom-right">
                        <div class="footer-bottom-inner">
                            <?php if(is_active_sidebar( 'footer2-column2' )){ 
                                dynamic_sidebar('footer2-column2');
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- .site-footer -->