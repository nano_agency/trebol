<?php
/**
 * The template for displaying the footer
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */
?>
<footer id="na-footer" class="na-footer footer-1">
    <!--    Footer center-->
    <?php  if(is_active_sidebar( 'footer1-column1' ) || is_active_sidebar( 'footer1-column2' )){ ?>
        <div class="footer-top clearfix">
            <div class="container">
                <div class="container-inner">
                    <div class="row">
                        <div class="col-md-4 col-sm-6">
                            <?php dynamic_sidebar('footer1-column1'); ?>
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <?php dynamic_sidebar('footer1-column2'); ?>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <?php dynamic_sidebar('footer1-column3'); ?>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <?php dynamic_sidebar('footer1-column4'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php }?>
    <!--    Footer bottom-->
    <div class="footer-bottom clearfix">
        <div class="container">
            <div class="container-inner">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 footer-bottom-left">
                        <div class="footer-bottom-inner">
                            <?php if(is_active_sidebar( 'footer-bottom-left' )){ 
                                dynamic_sidebar('footer-bottom-left');
                            }else{?>
                                <?php echo '<span>'.esc_html('&copy; Copyrights ','trebol').' '.date("Y").'<a href="'.esc_url('http://trebol.nanoagency.co').'">'.esc_html('  trebol theme. ','trebol').'</a>'; ?>
                            <?php }?>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 footer-bottom-right">
                        <div class="footer-bottom-inner">
                            <?php if(is_active_sidebar( 'footer-bottom-right' )){ 
                                dynamic_sidebar('footer-bottom-right');
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</footer><!-- .site-footer -->
