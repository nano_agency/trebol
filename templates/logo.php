<?php
    if(!get_theme_mod('trebol_logo')) {?>
        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
    <?php }
    else { ?>
        <div class="site-logo" id="logo">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                <?php
                    $trebol_logo = get_post_meta(get_the_ID(), 'trebol_logo', true);
                if($trebol_logo && !empty($trebol_logo)){?>
                    <img src="<?php echo esc_url(wp_get_attachment_url($trebol_logo)); ?>" alt="<?php echo esc_attr('logo'); ?>" />
                <?php }
                else {?>
                    <img src="<?php echo esc_url(get_theme_mod('trebol_logo')); ?>" alt="<?php bloginfo( 'name' ); ?>" />
                <?php }
                ?>
            </a>
        </div>
        <?php if(get_theme_mod('trebol_logo_retina')) { ?>
            <div class="site-logo" id="logo-retina"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo esc_url(get_theme_mod('trebol_logo_retina')); ?>" alt="<?php bloginfo( 'name' ); ?>" /></a></div>
        <?php } ?>
    <?php }
    ?>
<?php
if ( display_header_text() ) {
    $description = get_bloginfo( 'description', 'display' );
    if ( $description || is_customize_preview() ) : ?>
        <p class="site-description"><?php echo esc_attr($description); ?></p>
    <?php endif;
}
?>
