<?php
/**
 * @package     NA Core
 * @version     2.0
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

?>
<div class="header-topbar">
    <div id="trebol-top-navbar" class="top-navbar">        
        <div class="topbar-container text-center">
            <div class="topbar-cell">
                <div class="na-topbar">
                    <?php
                        if(is_active_sidebar( 'custom-topbar' )){
                            dynamic_sidebar('custom-topbar');
                        }                        
                    ?>
                </div>
            </div>
        </div>           
    </div>  
</div>