<?php 

$orig_post = $post;

$categories = get_the_category($post->ID);

if ($categories) {

	$category_ids = array();

	foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
	
	$args = array(
		'category__in'        => $category_ids,
		'post__not_in'        => array($post->ID),
		'posts_per_page'      => 3, 
		'ignore_sticky_posts' => 1,
		'orderby'             => 'rand'
	);

	$my_query = new wp_query( $args );
	if( $my_query->have_posts() ) { ?>
		<div class="archive-blog post-related">
			<h4 class="widgettitle">
				<?php esc_html_e('You may also like', 'trebol'); ?>
			</h4>
			<div class="row">
				<?php while( $my_query->have_posts() ) {
					$my_query->the_post();?>
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 description-hidden">
							<?php get_template_part( 'templates/layout/content-grid'); ?>
						</div>
				<?php } ?>
			</div>
		</div>
	<?php }
}
$post = $orig_post;
wp_reset_postdata();

?>