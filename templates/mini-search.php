<div class="searchform-wrap search-transition-wrap trebol-hidden">
    <div class="search-transition-inner">
        <?php
        get_search_form();
        ?>
        <button class="btn-mini-close pull-right">
            <span class="ion-android-close icons"></span>
        </button>
    </div>
</div>