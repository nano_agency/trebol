<div class="canvas-aside">
    <div class="canvas-aside-inner">
        <span class="btn-close">
            <i class="ion-android-close icons"></i>            
        </span>
        <div class="canvas-aside-wrap">
            <?php if(is_active_sidebar( 'custom-intro-sidebar' )){ 
                dynamic_sidebar('custom-intro-sidebar');
            } ?>
        </div>
    </div>
</div>