<div class="post-author">		
	<div class="author-img">
		<?php echo get_avatar( get_the_author_meta('email'), '100' ); ?>
	</div>	
	<div class="author-content">
		<h5 class="author-content-name"><?php the_author_posts_link(); ?></h5>
        <p class="author-content-description"><?php the_author_meta('description'); ?></p>
        <div class="author-content-social">
        	<?php if(get_the_author_meta('facebook')) : ?><a target="_blank" class="author-social" href="<?php echo esc_url('http://facebook.com/');?><?php echo the_author_meta('facebook'); ?>"><i class="ion-social-facebook icons"></i></a><?php endif; ?>
        	<?php if(get_the_author_meta('twitter')) : ?><a target="_blank" class="author-social" href="<?php echo esc_url('http://twitter.com/');?><?php echo the_author_meta('twitter'); ?>"><i class="ion-social-twitter icons"></i></a><?php endif; ?>
        	<?php if(get_the_author_meta('instagram')) : ?><a target="_blank" class="author-social" href="<?php echo esc_url('http://instagram.com/');?><?php echo the_author_meta('instagram'); ?>"><i class="ion-social-instagram icons"></i></a><?php endif; ?>
        	<?php if(get_the_author_meta('google')) : ?><a target="_blank" class="author-social" href="<?php echo esc_url('http://plus.google.com/');?><?php echo the_author_meta('google'); ?>?rel=author"><i class="ion-social-google icons"></i></a><?php endif; ?>
	        <?php if(get_the_author_meta('pinterest')) : ?><a target="_blank" class="author-social" href="<?php echo esc_url('http://pinterest.com/');?><?php echo the_author_meta('pinterest'); ?>"><i class="ion-social-pinterest icons"></i></a><?php endif; ?>	
        </div>
	</div>	
</div>