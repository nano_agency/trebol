<?php
$trebol_cart      = get_theme_mod('trebol_cart',false);
?>
<div class="trebol-header-placeholder hidden-xs hidden-sm hidden-md placeholder-<?php echo esc_attr($keepMenu);?>"></div>
<header id="masthead" class="site-header header-vertical hidden-xs hidden-sm hidden-md">
    <div id="trebol-header" class="trebol-header">
        <div class="header-container">
                <div class="trebol-header-content">
                    <!--Logo-->
                    <div class="header-content-logo">
                        <?php
                            get_template_part('templates/logo');
                        ?>
                    </div>
                    <!--Seacrch & Cart-->
                    <div class="header-content-right">
                        <div class="header-nav--right searchform-mini">
                            <button class="btn-mini-search"><i class="icon-magnifier icons"></i></button>
                        </div>

                        <?php if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) { ?>
                            <div class="header-nav--right header-act">
                                <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')); ?>">
                                    <span class="hidden"><?php esc_html_e('My Account', 'trebol'); ?></span>
                                    <span class="icon-user"></span>
                                </a>
                            </div>
                        <?php } ?>

                        <?php if($trebol_cart):?>
                            <div class="header-nav--right cart-wrap">
                                <?php
                                if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) { ?>
                                    <?php trebol_cartbox();?>
                                <?php }
                                ?>
                            </div>
                        <?php endif;?>

                        <div class="header-nav--right na-side-menu">
                            <button class="side-menu-button" type="button">
                                <span class="icon ion-android-menu side-menu-icon"></span>
                            </button>
                        </div>

                    </div>

                    <!-- Menu-->
                    <div class="header-content-menu">
                        <div class="menu-vertical">
                            <div id="na-menu-primary" class="nav-menu clearfix">
                                <nav class="na-menu-primary clearfix">
                                    <?php
                                    if (has_nav_menu('primary_navigation')) :
                                        // Main Menu
                                        wp_nav_menu( array(
                                            'theme_location' => 'primary_navigation',
                                            'menu_class'     => 'nav navbar-nav na-menu mega-menu',
                                            'container_id'   => 'vertical-primary',
                                            'walker'         => new Trebol_Menu_Maker_Walker()
                                        ) );
                                    endif;
                                    ?>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <div class="footer-bottom-inner">
        <?php if(is_active_sidebar( 'footer-bottom-right' )){
            dynamic_sidebar('footer-bottom-right');
        } ?>

        <?php if(get_theme_mod('trebol_copyright_text')) {?>
            <span><?php echo  wp_kses_post(get_theme_mod('trebol_copyright_text'));?></span>
        <?php }else{?>
            <?php echo '<span>'.esc_html('&copy; Copyrights ','trebol').' '.date("Y").'<a href="'.esc_url('http://trebol.nanoagency.co').'">'.esc_html('  Trebol. All rights reserved','trebol').'</a>'; ?>
        <?php }?>

    </div>
</header><!-- .site-header -->

