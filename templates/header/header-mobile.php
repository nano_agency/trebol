<?php
/**
 * The template for displaying Header mobile
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */
$trebol_cart = get_theme_mod('trebol_cart',false);
$keepMenu    = str_replace('','',trebol_keep_menu() );

?>
<div id="moblie-header" class="moblie-header hidden-lg <?php echo esc_attr($keepMenu);?>">
    <div class="moblie-header-element">
        <!--Logo-->
        <div class="header-moblie-logo">
            <?php
                get_template_part('templates/logo');
            ?>
        </div>
        <div class="header-mobile-content">

            <?php if($trebol_cart):?>
                <div class="cart-mobile">
                    <?php
                    if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) { ?>
                        <?php trebol_cartbox();?>
                    <?php }
                    ?>
                </div>
            <?php endif;?>

            <!-- Menu-->
            <button type="button" class="menu-mobile-icon menu-mobile-toggle">
                <i class="icon ion-android-menu"></i>
            </button>
        </div>
    </div>
    <div class="nav-mobile-menu">
        <div class="nav-mobile-menu-inside">            
            <div class="search-mobile">
                <div class="search-mobile-inner">
                    <?php
                    get_search_form();
                    ?>
                </div>
            </div>
            <nav class="menu-mobile-primary">
                <nav class="na-menu-primary clearfix">
                    <?php
                    if (has_nav_menu('primary_navigation')) :
                        // Main Menu
                        wp_nav_menu( array(
                            'theme_location' => 'primary_navigation',
                            'menu_class'     => 'nav navbar-nav na-menu mega-menu',
                            'container_id'   => 'mobile-primary',
                            'walker'         => new Trebol_Menu_Maker_Walker()
                        ) );
                    endif;
                    ?>
                </nav>
            </nav>
        </div>
    </div>
</div>
<!-- .site-header -->