<?php
    global $product;
    $trebol_woo_cart  =   get_theme_mod( 'trebol_woo_cart',false);

    if(isset($_GET['cart'])){
        $trebol_woo_cart=$_GET['cart'];
    }
    $add_class="no-cart";
    if (isset($trebol_woo_cart) && ($trebol_woo_cart !='false')){ $add_class='show-cart'; }
?>

<div class="product-block product inner-product-content">
    <figure class="caption-image product-image">
        <?php
            do_action( 'woocommerce_before_shop_loop_item' );
                do_action( 'woocommerce_before_shop_loop_item_title' );
            do_action( 'woocommerce_after_shop_loop_item' );
        ?>
        <div class="button-groups clearfix">
            <?php do_action('trebol_yith_wishlist' );?>
        </div>
    </figure>
    
    <div class="caption-product <?php echo esc_attr($add_class);?>">
        <div class="caption">
            <h3 class="product-name">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </h3>
            <div class="action-link-hide">
                <?php if (isset($trebol_woo_cart) && ($trebol_woo_cart !='false')):?>
                    <div class="ground-addcart">
                        <?php do_action( 'woocommerce_add_to_cart_item' ); ?>
                    </div>
                <?php endif;?>
                <div class="product-price">
                    <?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
                </div>         
            </div>
        </div>          
    </div>
</div>