<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
    return;
}
$trebol_layout_product      = get_theme_mod('trebol_layout_product','grid');
$trebol_space_product      = get_theme_mod('trebol_space_product','padding15px');

if(isset($_GET['space-product'])){
        $trebol_space_product=$_GET['space-product'];
}
if(isset($_GET['product-layouts'])){
        $trebol_layout_product=$_GET['product-layouts'];
}

?>

<li <?php post_class('col-item item-'.$trebol_layout_product.' item-'.$trebol_space_product ); ?>>
    <?php wc_get_template_part( 'layouts/content-product',$trebol_layout_product); ?>
</li>