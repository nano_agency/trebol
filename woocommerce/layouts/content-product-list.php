<?php
    global $product;
    $trebol_woo_cart  =   get_theme_mod( 'trebol_woo_cart',false);
     if(isset($_GET['cart'])){
        $trebol_woo_cart=$_GET['cart'];
    }
    $add_class="no-cart";
    if (isset($trebol_woo_cart) && ($trebol_woo_cart !='false')){ $add_class='show-cart'; }
?>
<div class="product-block product-list product inner-product-content">
    <div class="product-list-content">
        <figure class="caption-image product-image">
            <?php
                do_action( 'woocommerce_before_shop_loop_item' );
                    do_action( 'woocommerce_before_shop_loop_item_title' );
                do_action( 'woocommerce_after_shop_loop_item' );
            ?>
        </figure>        
        <div class="caption-product <?php echo esc_attr($add_class);?>">
            <div class="caption">
                <h3 class="product-name">
                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                </h3>
                <div class="product-price">
                    <?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
                </div>
                <div class="description-product">
                    <?php woocommerce_template_single_excerpt();?>
                </div>
            </div>
            <div class="clearfix">
                <?php if (isset($trebol_woo_cart) && ($trebol_woo_cart !='false')):?>
                    <div class="ground-addcart pull-left">
                        <?php do_action( 'woocommerce_add_to_cart_item' ); ?>
                    </div>
                <?php endif;?>
                <div class="button-groups pull-left">
                    <?php do_action('trebol_yith_wishlist' );?>
                </div>    
            </div>
        </div>
    </div>
</div>