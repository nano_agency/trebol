<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
$class='';
$display_type              = get_option( 'woocommerce_shop_page_display', '' );

$shopLayouts               = get_theme_mod('trebol_layout_woocat', 'container');
$shopTop                   = get_theme_mod('trebol_top_shop',false);
$trebol_space_product      = get_theme_mod('trebol_space_product','padding15px');

//get url
if(isset($_GET['space-product'])){
        $trebol_space_product=$_GET['space-product'];
}
if(isset($_GET['layout-woocat'])){
        $shopLayouts=$_GET['layout-woocat'];
}
if(isset($_GET['top-shop'])){
        $shopTop=$_GET['top-shop'];
}
if(isset($_GET['shop-type'])){
        $display_type=$_GET['shop-type'];
}

if($shopTop){
    $class='hidden';
}

get_header( 'shop' ); ?>
<section class="wrap-breadcrumb <?php echo esc_attr($class);?>">
    <div class="<?php echo esc_attr($shopLayouts);?>">
        <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
            <h1 class="page-title"><?php woocommerce_page_title(); ?></h1>
        <?php endif; ?>
        <?php trebol_woocommerce_breadcrumb(); ?>
    </div>
</section>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="wrap-product">            
            <div class="<?php echo esc_attr($shopLayouts); ?> container-<?php echo esc_attr($trebol_space_product);?>">
                <div class="row shop-content type-loadShop trebol-<?php echo esc_attr($display_type);?> clearfix">
                    <?php do_action('woo-sidebar-left'); ?>
                    <?php do_action('woo-content-before'); ?>

                        <?php if ( woocommerce_product_loop() )  { ?>

                                <?php if($display_type=='subcategories' || $display_type=='both' ){?>
                                    <div class="shop-subcategories clearfix">
                                        <?php trebol_subcategories_loop_start();?>
                                    </div>
                                <?php  }?>

                                <div class="shop-content-top clearfix <?php echo esc_attr($class);?>">
                                    <?php
                                    /**
                                     * woocommerce_before_shop_loop hook.
                                     *
                                     * @hooked wc_print_notices - 10
                                     * @hooked trebol_nav_category - 17
                                     * @hooked trebol_nav_filters - 18
                                     * @hooked trebol_btn_filter - 19
                                     * @hooked woocommerce_catalog_ordering - 30
                                     * @hooked trebol_filter_down - 31
                                     * @hooked trebol_search - 32
                                     * @hooked trebol_filter_full - 33
                                     */
                                    do_action( 'woocommerce_before_shop_loop' );
                                    ?>
                                </div>
                                <?php  woocommerce_product_loop_start();
                                if ( wc_get_loop_prop( 'total' ) ) {

                                    while ( have_posts() ) {
                                        the_post();

                                        /**
                                         * Hook: woocommerce_shop_loop.
                                         *
                                         * @hooked WC_Structured_Data::generate_product_data() - 10
                                         */

                                        do_action( 'woocommerce_shop_loop' );

                                        wc_get_template_part( 'content', 'product' );
                                    }

                                }
                                woocommerce_product_loop_end(); ?>
                                <?php
                                /**
                                 * woocommerce_after_shop_loop hook.
                                 *
                                 * @hooked woocommerce_pagination - 10
                                 */
                                do_action( 'woocommerce_after_shop_loop' );
                                ?>

                        <?php } else {
                        /**
                         * Hook: woocommerce_no_products_found.
                         *
                         * @hooked wc_no_products_found - 10
                         */
                        do_action( 'woocommerce_no_products_found' );
                        } ?>

                    <?php do_action('woo-content-after'); ?>
                </div>
            </div>
        </div>
    </main>
</div>

<?php
/**
 * woocommerce_sidebar hook.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );
?>

<?php get_footer( 'shop' ); ?>

