<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
$style_css='';
$bg_entry                  = get_theme_mod('trebol_detail_bg_entry','');
if($bg_entry){
        $bg_entry           ="background:$bg_entry";
        $style_css          ='style = '.$bg_entry.'';
}

?>

<?php
    /**
     * woocommerce_before_single_product hook
     *
     * @hooked wc_print_notices - 10
     */
     do_action( 'woocommerce_before_single_product' );

     if ( post_password_required() ) {
        echo get_the_password_form();
        return;
     }
?>
<div id="product-<?php the_ID(); ?>" <?php post_class('product-layout-background'); ?> >

    <div class="product-detail-wrap">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="summary-before" <?php echo esc_html($style_css);?>>
                    <?php
                        /**
                         * woocommerce_before_single_product_summary hook
                         *
                         * @hooked woocommerce_show_product_sale_flash - 10
                         * @hooked trebol_show_product_images_carousel - 20
                         */
                        do_action( 'trebol_before_single_product_summary_background' );
                    ?>
                </div>                
            </div>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="container text-center">
                    <div class="summary entry-summary trebol_summary">
                        <div class="clearfix trebol_woocommerce_breadcrumb">
                            <?php trebol_woocommerce_breadcrumb(); ?>
                            <?php if ( is_product() ){ ?>
                                <div class="product-nav style-2">
                                    <?php trebol_nav_product(); ?>
                                </div>
                            <?php }?> 
                        </div>
                        <?php
                            /**
                             * woocommerce_single_product_summary hook
                             *
                             * @hooked woocommerce_template_single_title - 5
                             * @hooked woocommerce_template_single_rating - 10
                             * @hooked woocommerce_template_single_price - 10
                             * @hooked woocommerce_template_single_excerpt - 20
                             * @hooked woocommerce_template_single_add_to_cart - 30
                             * @hooked woocommerce_template_single_meta - 40
                             * @hooked woocommerce_template_single_sharing - 50
                             * @hooked WC_Structured_Data::generate_product_data() - 60
                             */
                            do_action( 'woocommerce_single_product_summary' );
                        ?>
                    </div>
                </div>                                
            </div>
        </div>
    </div><!-- .product-detail-wrap -->
    <div class="product-detail-carousel-tab">
        <div class="container">
            <?php
                /**
                * woocommerce_after_single_product_summary hook
                *
                * @hooked woocommerce_output_product_data_tabs - 10
                * @hooked woocommerce_upsell_display - 15
                * @hooked woocommerce_output_related_products - 20
                * @hooked trebol_woocommerce_recently_viewed_products - 21
                */
                do_action( 'woocommerce_after_single_product_summary' );
            ?>
        </div>
    </div><!-- .product-detail-carousel-tab -->

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
