<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */
global $woocommerce;
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
$woo_recently                   = get_theme_mod('trebol_woo_recently_viewed',false);
if($woo_recently){?>
    <div class="tabs-related-block">
        <ul class="nav nav-tabs" id="tabs-related" role="tablist">
            <li class="nav-item active">
                <a class="nav-link " id="related-tab" data-toggle="tab" href="#related" role="tab" aria-controls="home" aria-selected="true"><?php esc_html_e( 'Related products', 'trebol' ); ?></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="recently-tab" data-toggle="tab" href="#recently" role="tab" aria-controls="recently" aria-selected="false"><?php esc_html_e( 'Recently viewed', 'trebol' ); ?></a>
            </li>
        </ul>
        <div class="tab-content" id="tabs-related-content">
            <div class="tab-pane fade active" id="related" role="tabpanel" aria-labelledby="related-tab">
                <?php
                if ( $related_products ) : ?>
                    <div class="related-wrapper">
                        <div class="products-block">
                            <div class="row">
                                <?php foreach ( $related_products as $related_product ) : ?>
                                    <?php
                                    $post_object = get_post( $related_product->get_id() );
                                    setup_postdata( $GLOBALS['post'] =& $post_object );
                                    wc_get_template_part( 'content', 'product-related' ); ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                <?php endif;
                wp_reset_postdata();
                ?>
            </div>
            <div class="tab-pane fade" id="recently" role="tabpanel" aria-labelledby="recently-tab">
                <div class="recently-wrapper">
                    <div class="products-block">
                        <div class="row">
                            <?php do_action('woocommerce_recently_viewed_products');?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } else {
    if ( $related_products ) : ?>
    <div class="widget widget-related products-block">
        <h2 class="widgettitle"><?php esc_html_e( 'Related products', 'trebol' ); ?></h2>

        <div class="related-wrapper">
            <div class="products-block">
                <div class="na-carousel row" data-number="4" data-table="2" data-mobile="2" data-mobilemin="2" data-arrows="false">

                    <?php foreach ( $related_products as $related_product ) : ?>

                        <?php
                        $post_object = get_post( $related_product->get_id() );

                        setup_postdata( $GLOBALS['post'] =& $post_object );

                        wc_get_template_part( 'content', 'product-related' ); ?>

                    <?php endforeach; ?>

                </div>
            </div>
        </div>

    </div>
    <?php endif;
    wp_reset_postdata();
 }?>