<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */
global $woocommerce_loop;
global $wp_query;

$cate   					= get_queried_object();
//number products on the row is 4
$col 						= get_theme_mod('trebol_woo_product_per_row','4');
//check layout product for the row
$trebol_layout_product      = get_theme_mod('trebol_layout_product','grid');
$trebol_space_product      = get_theme_mod('trebol_space_product','padding15px');


//get url
if(isset($_GET['col'])){
        $col=$_GET['col'];
}
if(isset($_GET['space-product'])){
        $trebol_space_product=$_GET['space-product'];
}
if(isset($_GET['product-layouts'])){
        $trebol_layout_product=$_GET['product-layouts'];
}

//product-layout
if(isset($trebol_layout_product) && $trebol_layout_product === 'list'){
	$col=1;
}
$display_type = get_option( 'woocommerce_shop_page_display', '' );
if(isset($_GET['subcat-type'])){
        $display_type=$_GET['subcat-type'];
}
?>

<ul class="products-block row affect-isotope columns-<?php echo esc_attr( $col); ?> row-<?php echo esc_attr($trebol_space_product);?> clearfix"
    data-col="<?php echo esc_attr($col);?>"
    data-paged="<?php echo esc_attr($wp_query->max_num_pages);?>"
    >