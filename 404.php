<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

get_header();

?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="site-main">
            <div class="container">
                <section class="error-404 not-found">                    
                    <header class="page-header">   
                        <div class="page-icon">
                            <span class="icon-link"></span>                         
                        </div>                     
                        <h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.','trebol' ); ?></h1>
                    </header><!-- .page-header -->
                    <div class="page-content">
                        <p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try a search?', 'trebol'); ?></p>
                        <?php get_search_form(); ?>
                    </div><!-- .page-content -->
                </section><!-- .error-404 -->
            </div>
        </div><!-- .site-main -->
    </main>   
</div><!-- .content-area -->

<?php get_footer(); ?>