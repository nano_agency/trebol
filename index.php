<?php
/**
 * The template for displaying Index pages
 *
 * @author      NanoAgency
 * @link        http://nanoagency.co
 * @copyright   Copyright (c) 2015 NanoAgency
 * @license     GPL v2
 */

get_header();
?>
    <div class="wrap-content container" role="main">
        <div class="row content-category">
            <?php do_action('archive-sidebar-left'); ?>
            <?php do_action('archive-content-before'); ?>
            <?php if ( have_posts() ) : ?>
                <div class="archive-blog row column-2">
                        <div class="affect-isotope clearfix">
                            <?php
                            // Start the Loop.
                            while ( have_posts() ) : the_post(); ?>
                                <div class="item-post col-item col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <?php get_template_part( 'templates/layout/content-list'); ?>
                                </div>
                            <?php endwhile;?>
                        </div>

                </div>
            <?php else :
            // If no content, include the "No posts found" template.
            get_template_part( 'content', 'none' );
            endif;?>

            <?php the_posts_pagination( array(
                'prev_text'          => '<i class="ion-ios-arrow-left"></i>',
                'next_text'          => '<i class="ion-ios-arrow-right"></i>',
                'before_page_number' => '<span class="meta-nav screen-reader-text"></span>',
            ) );
            ?>

            <?php do_action('archive-content-after'); ?>
            <?php do_action('archive-sidebar-right'); ?>
        </div>
        <!-- .content-area -->
    </div>

<?php
get_footer();