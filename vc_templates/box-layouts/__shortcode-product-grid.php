<?php
$query_shop = new WP_Query( apply_filters( 'woocommerce_shortcode_products_query', $atts ) );
$query_shop->query( $atts );
?>

<div class="products_shortcode_wrap items-grid type-tabShop main-content">
    <ul class="products-block tab-isotope row clearfix" data-col="<?php echo esc_attr($atts['column']);?>" data-paged="<?php echo esc_attr($query_shop->max_num_pages);?>">
        <?php while ( $query_shop->have_posts() ){
            $query_shop->the_post();?>
            <li <?php post_class('col-item'); ?>>
                <?php wc_get_template_part( 'layouts/content-product-grid'); ?>
            </li>
        <?php }
            wp_reset_postdata();
        ?>
    </ul>
</div>