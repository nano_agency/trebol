<?php

// ADD attributes Parallax
add_action( 'vc_after_init', 'trebol_add_parallax_param' );
function trebol_add_parallax_param() {
    $setting = array(
        'type' => 'dropdown',
        'heading' => __( 'Parallax', 'trebol' ),
        'param_name' => 'parallax',
        'value' => array(
            __( 'None', 'trebol' )      => '',
            __( 'Simple', 'trebol' )    => 'content-moving',
            __( 'Image', 'trebol' )     => 'content-moving-image',
            __( 'With fade', 'trebol' ) => 'content-moving-fade',
        ),
        'description' => __( 'Add parallax type background for row (Note: If no image is specified, parallax will use background image from Design Options).', 'trebol' ),
        'dependency' => array(
            'element' => 'video_bg',
            'is_empty' => true,
        ),
    );
    vc_add_param( 'vc_row', $setting );
}


