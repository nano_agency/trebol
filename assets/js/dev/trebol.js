(function($){
    "use strict";
    jQuery(document).ready(function(){

        jQuery(".owl-single").each(function(){
            var rtl = jQuery(this).data('rtl');
            jQuery(this).slick({
                autoplay: true,
                dots: true,
                rtl:rtl,
                autoplaySpeed: 2000,
            });
        });
        jQuery(".article-carousel").each(function(){
            var number = jQuery(this).data('number');
            var dots = jQuery(this).data('dots');
            var arrows = jQuery(this).data('arrows');
            var table = jQuery(this).data('table');
            var rtl = jQuery(this).data('rtl');
            var mobile = jQuery(this).data('mobile');
            var mobilemin = jQuery(this).data('mobilemin');

            jQuery(this).slick({                
                lazyLoad: 'ondemand',
                dots: dots,
                rtl:rtl,
                slidesToShow: number,
                arrows: arrows,
                autoplaySpeed: 4000,
                responsive: [
                    {
                        breakpoint: 900,
                        settings: {
                            slidesToShow: table
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: mobile
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: mobilemin
                        }
                    }
                ]
            });
        });

        jQuery(".article-carousel-center").each(function(){
            var number = jQuery(this).data('number');
            var dots = jQuery(this).data('dots');
            var rtl = jQuery(this).data('rtl');
            var arrows = jQuery(this).data('arrows');
            jQuery(this).slick({
                dots: dots,
                lazyLoad: 'ondemand',
                slidesToShow: number,
                arrows: arrows,
                slidesToScroll: 1,
                //autoplay: true,
                rtl:rtl,
                autoplaySpeed:5000,
                centerMode: true,
                variableWidth: true,
                focusOnSelect: true,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            variableWidth: false
                        }
                    },
                    {
                        breakpoint: 770,
                        settings: {
                            centerMode: false,
                            variableWidth: false
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            centerMode: false,
                            variableWidth: false
                        }
                    }
                ]

            });
        });

        //testimonial
        jQuery('.box-testimonial-wrap').slick({                       
            slidesToShow: 1,
            speed: 600,
            arrows: false,
            dots: false,            
            infinite: true,
            autoplay: true,
            autoplaySpeed: 3000
        });
        //box-category-slider
        jQuery(".box-category-slider").each(function(){            
            var dots = jQuery(this).data('dots');
            var arrows = jQuery(this).data('arrows');                       
            jQuery(this).slick({
                slidesToShow: 1,
                speed: 600,
                arrows: arrows,
                dots: dots,                                
                autoplaySpeed: 3000,
                prevArrow: jQuery('.prev-slide'),
                nextArrow: jQuery('.next-slide')
            });
        });
        //Block Intro
        jQuery('.block-intro-carousel').slick({                       
            slidesToShow:2,
            speed: 300,
            arrows: true,            
            dots: false,  
            prevArrow: jQuery('.prev-slide'),
            nextArrow: jQuery('.next-slide'),                      
            responsive: [{ 
                breakpoint: 767,
                settings: {
                    centerMode: true,                    
                    arrows: true,
                    dots: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    swipe: true
                } 
            }]  
        });

        //Countdown timer --------------------------------------------------------------------------------------------- /
        jQuery( '.na-timer' ).each(function(){
            var time = jQuery(this).data('end-date');
            jQuery( this ).countdown( time, function( event ) {
                jQuery( this ).html( event.strftime(''
                    + '<span class="countdown-days">%-D <span>days</span></span> '
                    + '<span class="countdown-hours">%H <span>hours</span></span> '
                    + '<span class="countdown-min">%M <span>mins</span></span> '
                    + '<span class="countdown-sec">%S <span>sec</span></span>'));
            });
        });



        // Parallax
        jQuery(".js-vc_parallax-o-image").each(function(){
            var parallax_image_src = jQuery(this).data('vc-parallax-image');
            jQuery(this).css( 'background-image', 'url(' + parallax_image_src + ')' );
            jQuery(this).parallax("50%", 0.0);
        });

        // Sticky Menu ------------------------------------------------------------------------------------------------/
            jQuery(".btn-mini-search").on( 'click', function(){
                jQuery(".searchform-wrap").removeClass('trebol-hidden');
            });
            jQuery(".btn-mini-close").on( 'click', function(){
                jQuery(".searchform-wrap").addClass('trebol-hidden');
            });

        // Menu Mobile ------------------------------------------------------------------------------------------------/

        jQuery("#moblie-header .menu-mobile-icon").live( 'click', function(e){
            e.preventDefault()
            jQuery('#moblie-header .nav-mobile-menu').slideToggle('100','linear');
        });
        // Sticky Menu
        var scrollTop = $(document).scrollTop();
        var headerHeight = $('.header-fixed').outerHeight();
        var contentHeight = $('.site-header').outerHeight();
        jQuery(".placeholder-header-fixed").each(function() {
            jQuery(this).css('height', headerHeight);
        });

        $(window).scroll(function() {
            var headerscroll = $(document).scrollTop();

            if (headerscroll > contentHeight){
                jQuery('.header-fixed').addClass('fixed');
                jQuery('.header-fixed').css('transform','translateY(-'+contentHeight+'px)');
                jQuery('.header-fixed').css('transition','all 0.3s');

            }else{
                jQuery('.header-fixed').removeClass('fixed');
            }
            //
            if (headerscroll > scrollTop){
                $('.header-fixed').removeClass('scroll-top');

            }
            else {
                $('.header-fixed').addClass('scroll-top');
                jQuery('.header-fixed').css('transform','translateY(0px)');
                jQuery('.header-fixed').css('transition','all 0.3s');
            }
            scrollTop = $(document).scrollTop();
        });
        // Quantity ---------------------------------------------------------------------------------------------------/
        jQuery(".form-inline .quantity").each(function(){
            var self    =  jQuery(this);
            var p       = self.find('.qty-plus');
            var m       = self.find('.qty-minus');
            p.on( 'click', function(){
                    self.find('.qty').val( parseInt( self.find('.qty').val()) + 1 );
                    self.find('.qty').trigger( 'change' );
            } );
            m.on( 'click', function(){
                if( parseInt(self.find('.qty').val())  > 1 ) {
                    self.find('.qty').val( parseInt( self.find('.qty').val()) - 1 );
                    self.find('.qty').trigger( 'change' );
                }
            } );

        });


        // Woo --------------------------------------------------------------------------------------------------------/
        //carousel
        jQuery(".na-carousel").each(function(){
            var number = jQuery(this).data('number');
            var auto = jQuery(this).data('auto');
            var pagination =jQuery(this).data('pagination');
            jQuery(this).slick({
                autoplay: auto,
                slidesToShow: number,
                slidesToScroll: 1,
                dots:pagination,                      
                prevArrow:"<button type='button' class='prev-slide'><i class='icon ion-android-arrow-back'></i><span>Previous</span></button>",
                nextArrow:"<button type='button' class='next-slide'><span>Next</span><i class='icon ion-android-arrow-forward'></i></button>",
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: true,
                            dots: true
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        });
        //tab-carousel
        jQuery('.tab-carousel').slick({
            autoplay: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false,
            fade: true,
            ocusOnSelect: true,
        });

        //Login
        jQuery('.myaccount-login').each(function(){
            jQuery('.btn-form-login').on('click',function() {
                $('.login-wrap').slideUp(0);
                $('.register-wrap').slideDown(250);
            });
            jQuery('.btn-form-register').on('click',function() {
                $('.register-wrap').slideUp(0);
                $('.login-wrap').slideDown(250);

            });

        });
        function widgetPanelShowCart (showLoader) {
            var $this = $(this);
            // $this.$widgetPanelOverlay = $('.canvas-overlay');
            $this.classWidgetPanelOpen = 'cart-box-open';
            $this.$body = $('body');

            if (showLoader) {
                $('#cart-panel-loader').addClass('show');
            }

            $this.$body.addClass($this.classWidgetPanelOpen);
            // $this.$widgetPanelOverlay.addClass('show');


        }

        $('.mini-cart').live('click',function() {
            $('body').toggleClass('cart-box-open');
        });
        $('.mini-cart-close').on('click',function() {
            $('body').removeClass('cart-box-open');
        });
       


        //Ajax Remove Cart ===================================
        $(document).on('click', '.rit_product_remove', function(event) {
            var $this = $(this);
            var product_key = $this.data('cart-item-key');

            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: ajaxurl,
                data: { action: "cart_remove_product",
                    product_key: product_key
                },success: function(data){
                    var $cart = $('.cart-box');

                    if(data.cart_count==0){
                        $cart.find('.cart_list').html('<li class="empty">'+$cart.find('.cart_container').data('text-emptycart')+'</li>');
                        $cart.find('.cart-bottom').remove();
                    }else{
                        $cart.find('.total .amount').remove();
                        $('.mini-cart-subtotal').html(data.cart_subtotal);
                        $this.parent().remove();
                    }
                    $('.text-items .mini-cart-items').html(data.cart_count);
                }
            });
            return false;
        });

        //Ajax Add to Cart ===================================
        $(document).on('click', '.add_to_cart_button', function(event) {
            widgetPanelShowCart(true);
            setTimeout(function() {
                widgetPanelHideCartLoader(true); // Args: (showLoader)
            }, 350);
            jQuery("html, body").animate({ scrollTop: 0 }, 300);
        });
        //Ajax Add to Cart Detail ===================================
        $(document).on('click', '.single_add_to_cart_button', function(event) {

            var $this = $(this);
            var $productForm = $this.closest('form');

            var	data = {
                product_id:				$productForm.find("[name*='add-to-cart']").val(),
                product_variation_data: $productForm.serialize()
            };
            if (!data.product_id) {
                console.log('(Error): No product id found');
                return;
            }
            widgetPanelShowCart(true);
            $.ajax({
                type: 'POST',
                dataType: 'html',
                url: '?add-to-cart=' + data.product_id + '& ajax-add-to-cart=1',
                data: data.product_variation_data,
                cache: false,
                headers: {'cache-control': 'no-cache'},
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log('AJAX error - SubmitForm() - ' + errorThrown);
                },
                success: function(response){
                    var $response = $('<div>' + response + '</div>'),
                        $shopNotices = $response.find('#shop-notices-wrap'), // Shop notices
                        hasError = ($shopNotices.find('.woocommerce-error').length) ? true : false,
                        cartHash = '';
//                      Get replacement elements/values

                    var fragments = {
                        '.mini-cart-items': $response.find('.mini-cart-items'), // Header menu cart count
                        '#shop-notices-wrap': $shopNotices,
                        '.cart-box': $response.find('.cart-wrap .cart-box') // Widget panel mini cart
                    };

                    // Replace elements
                    $.each(fragments, function(selector, $element) {
                        if ($element.length) {
                            $(selector).replaceWith($element);
                        }
                    });

                    setTimeout(function() {
                        widgetPanelHideCartLoader(true); // Args: (showLoader)
                    }, 550);
                    if (hasError) {
                        setTimeout(function() {
                            widgetPanelHiddenCart(true); // Args: (showLoader)
                        }, 500);
                    }
                }
            });
            return false;
        });


        var initPhotoSwipeFromDOM = function(gallerySelector) {

            // parse slide data (url, title, size ...) from DOM elements
            // (children of gallerySelector)
            var parseThumbnailElements = function(el) {
                var thumbElements = el.childNodes,
                    numNodes = thumbElements.length,
                    items = [],
                    figureEl,
                    linkEl,
                    size,
                    item;

                for(var i = 0; i < numNodes; i++) {

                    figureEl = thumbElements[i]; // <figure> element

                    // include only element nodes
                    if(figureEl.nodeType !== 1) {
                        continue;
                    }

                    linkEl = figureEl.children[0]; // <a> element

                    size = linkEl.getAttribute('data-size').split('x');

                    // create slide object
                    item = {
                        src: linkEl.getAttribute('href'),
                        w: parseInt(size[0], 10),
                        h: parseInt(size[1], 10)
                    };



                    if(figureEl.children.length > 1) {
                        // <figcaption> content
                        item.title = figureEl.children[1].innerHTML;
                    }

                    if(linkEl.children.length > 0) {
                        // <img> thumbnail element, retrieving thumbnail url
                        item.msrc = linkEl.children[0].getAttribute('src');
                    }

                    item.el = figureEl; // save link to element for getThumbBoundsFn
                    items.push(item);
                }

                return items;
            };

            // find nearest parent element
            var closest = function closest(el, fn) {
                return el && ( fn(el) ? el : closest(el.parentNode, fn) );
            };

            // triggers when user clicks on thumbnail
            var onThumbnailsClick = function(e) {
                e = e || window.event;
                e.preventDefault ? e.preventDefault() : e.returnValue = false;

                var eTarget = e.target || e.srcElement;

                // find root element of slide
                var clickedListItem = closest(eTarget, function(el) {
                    return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
                });

                if(!clickedListItem) {
                    return;
                }

                // find index of clicked item by looping through all child nodes
                // alternatively, you may define index via data- attribute
                var clickedGallery = clickedListItem.parentNode,
                    childNodes = clickedListItem.parentNode.childNodes,
                    numChildNodes = childNodes.length,
                    nodeIndex = 0,
                    index;

                for (var i = 0; i < numChildNodes; i++) {
                    if(childNodes[i].nodeType !== 1) {
                        continue;
                    }

                    if(childNodes[i] === clickedListItem) {
                        index = nodeIndex;
                        break;
                    }
                    nodeIndex++;
                }



                if(index >= 0) {
                    // open PhotoSwipe if valid index found
                    openPhotoSwipe( index, clickedGallery );
                }
                return false;
            };

            // parse picture index and gallery index from URL (#&pid=1&gid=2)
            var photoswipeParseHash = function() {
                var hash = window.location.hash.substring(1),
                    params = {};

                if(hash.length < 5) {
                    return params;
                }

                var vars = hash.split('&');
                for (var i = 0; i < vars.length; i++) {
                    if(!vars[i]) {
                        continue;
                    }
                    var pair = vars[i].split('=');
                    if(pair.length < 2) {
                        continue;
                    }
                    params[pair[0]] = pair[1];
                }

                if(params.gid) {
                    params.gid = parseInt(params.gid, 10);
                }

                return params;
            };

            var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
                var pswpElement = document.querySelectorAll('.pswp')[0],
                    gallery,
                    options,
                    items;

                items = parseThumbnailElements(galleryElement);

                // define options (if needed)
                options = {

                    // define gallery index (for URL)
                    galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                    getThumbBoundsFn: function(index) {
                        // See Options -> getThumbBoundsFn section of documentation for more info
                        var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                            pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                            rect = thumbnail.getBoundingClientRect();

                        return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
                    }

                };

                // PhotoSwipe opened from URL
                if(fromURL) {
                    if(options.galleryPIDs) {
                        // parse real index when custom PIDs are used
                        // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                        for(var j = 0; j < items.length; j++) {
                            if(items[j].pid == index) {
                                options.index = j;
                                break;
                            }
                        }
                    } else {
                        // in URL indexes start from 1
                        options.index = parseInt(index, 10) - 1;
                    }
                } else {
                    options.index = parseInt(index, 10);
                }

                // exit if index not found
                if( isNaN(options.index) ) {
                    return;
                }

                if(disableAnimation) {
                    options.showAnimationDuration = 0;
                }

                // Pass data to PhotoSwipe and initialize it
                gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
                gallery.init();
            };

            // loop through all gallery elements and bind events
            var galleryElements = document.querySelectorAll( gallerySelector );

            for(var i = 0, l = galleryElements.length; i < l; i++) {
                galleryElements[i].setAttribute('data-pswp-uid', i+1);
                galleryElements[i].onclick = onThumbnailsClick;
            }

            // Parse URL and open gallery if it contains #&pid=3&gid=1
            var hashData = photoswipeParseHash();
            if(hashData.pid && hashData.gid) {
                openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
            }
        };

// execute above function
        initPhotoSwipeFromDOM('.gallery-main');
        // CANVAS aside ------------------------------------------------------------------------------------------------/
        var menuWrap = jQuery('body').find('.side-menu-button'),
            mainWrapper = jQuery('body'),
            iconClose = jQuery('.canvas-aside .btn-close'),
            canvasOverlay = jQuery('.canvas-overlay');

            // Call Function Canvas
            menuWrap.on( 'click', function(){
               mainWrapper.toggleClass('canvas-open');
                canvasOverlay.toggleClass('show');
            });
            // Click icon close
            iconClose.on( 'click', function(){
               mainWrapper.toggleClass('canvas-open');
            canvasOverlay.toggleClass('show');
            });
            // Click canvas
            canvasOverlay.on( 'click', function(){
                if ( $(this).hasClass('show') && $('body').hasClass('canvas-open')) {
                    $(this).removeClass('show');
                    $('body').removeClass('canvas-open');
                }
            });
        
        // Filter -----------------------------------------------------------------------------------------------------/
        function filter (showLoader) {
            var $this = $(this);
            $this.$widgetPanelOverlay = $('.canvas-overlay');
            $this.classWidgetPanelOpen = 'cart-box-open';
            $this.$body = $('body');

            if (showLoader) {
                $('#cart-panel-loader').addClass('show');
            }

            $this.$body.addClass($this.classWidgetPanelOpen);
            $this.$widgetPanelOverlay.addClass('show');
        }

        $('.canvas-overlay').on('click',function() {
            if ( $(this).hasClass('show') && $('body').hasClass('filter-open')) {
                $(this).removeClass('show');
                $('body').removeClass('filter-open');
            }
        });
        $('.cart-panel-title .close').live('click',function() {
            $('.canvas-overlay').removeClass('show');
            $('body').removeClass('filter-open');
        });


        $('.btn-filter-left .btn-filter').live('click',function() {
            $('.canvas-overlay').toggleClass('show');
            $('body').toggleClass('filter-open');
        });

        $('.btn-filter-full .btn-filter').live('click',function() {
            $('.filter-full').slideToggle('200','linear');
            if ( $('.filter-full').hasClass('active') ) {
                $('.filter-full').removeClass('active');
            } else {
                $('.filter-full.active').removeClass('active');
                $('.filter-full').addClass('active');    
            }
        });

        function widgetPanelHideCartLoader (showLoader) {
            $('#cart-panel-loader').addClass('fade-out');
            setTimeout(function() { $('#cart-panel-loader').removeClass('fade-out show'); }, 500);
        }
    });

    //slider showcase
    var slideCount = null;
    $('.tab-carousel').on('init', function(event, slick){
        slideCount = slick.slideCount;
        var $el = $(this).find('.slide-count-wrap .total');
        $el.text(slideCount);
        var $els =$(this).find('.slide-count-wrap .current');
        $els.text(slick.currentSlide + 1);
    });
    $('.tab-carousel').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        var $els =$(this).find('.slide-count-wrap .current');
        $els.text(nextSlide + 1);
    });

})(jQuery);



