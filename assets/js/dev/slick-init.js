(function($){
    "use strict";
    jQuery(document).ready(function($) {
        //vertical
        jQuery('.product_vertical').each(function(){
            var number = jQuery(this).data('number');
            jQuery('.gallery-main').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                dots: false,
                asNavFor: '.gallery-nav',
                fade: true,
            });
            jQuery('.gallery-nav').slick({
                slidesToShow: number,
                slidesToScroll: 1,
                asNavFor: '.gallery-main',
                focusOnSelect: true,
                arrows: false,
                infinite: false,
                vertical: true,
                verticalSwiping: true
            });

        });
        //horizontal
        jQuery('.product_horizontal').each(function(){
            var number = jQuery(this).data('number');
            jQuery('.gallery-main').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                dots: false,
                asNavFor: '.gallery-nav',
                fade: true,
            });
            jQuery('.gallery-nav').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                asNavFor: '.gallery-main',
                focusOnSelect: true,
                arrows: false,
                infinite: false
            });

        });
        //carousel
        jQuery('.product_carousel').each(function() {
            var number = jQuery(this).data('number');
            jQuery('.gallery-main').slick({
                dots: false,
                slidesToShow: 3,
                arrows: true,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed:5000,
                centerMode: true,
                variableWidth: true,
                focusOnSelect: true,
            });
        });
        //background
        jQuery('.product_background').each(function(){
            var number = jQuery(this).data('number');
            jQuery('.gallery-main').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                centerMode: true,
                dots: false,
                asNavFor: '.gallery-nav',
                fade: true,
            });
            jQuery('.gallery-nav').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                asNavFor: '.gallery-main',
                focusOnSelect: true,
                arrows: false,
                infinite: false
            });

        });
        //sticky
        jQuery('.product_sticky').each(function() {
            jQuery('.sticky-product-detail').theiaStickySidebar({additionalMarginTop: 0, additionalMarginBottom: 0, minWidth: 992});
        });
		//carousel
        jQuery('.product_carousel_left').each(function() {
            var number = jQuery(this).data('number');
            jQuery('.product-gallery-slider').slick({
                dots: true,
                slidesToShow: 1,
                arrows: false,
                autoplaySpeed: 4000,
            });
        });
    });

})(jQuery);