(function($){
    "use strict";
    function trebol_isotope(){
        var $grid =$('.affect-isotope').isotope({
            transitionDuration: '0.4s',
            masonry: {
                columnWidth:'.col-item'
            },
            fitWidth: true,
        });
        $grid.imagesLoaded().progress( function() {
            $grid.isotope('layout');
        });
    }
    function trebol_isotope_percentage(){
        var $grid = $('.affect-isotope-cats').isotope({
            itemSelector: '.col-item',
            percentPosition: true,            
            masonry: {
                columnWidth: '.grid-sizer'
            }
        });
        $('.affect-isotope-cats.cat-1').isotope({
            layoutMode: 'fitRows'
        });

        $grid.imagesLoaded().progress( function() {
            $grid.isotope('layout');
        });
    }


    function resizeLayout() {
        var col              =jQuery('.products-block').data('col');
        var w_warp_product      =jQuery('.products-block').outerWidth();
        if(w_warp_product > 768 ) {
            var w_width   =Math.floor(w_warp_product/col);
        }
        if(w_warp_product < 769 ) {
            var w_width   =Math.floor(w_warp_product/2);
        }
        jQuery('.products-block .col-item').css({"width": (w_width-1)+"px","height":"auto"});
        trebol_isotope();
    }

    jQuery(document).ready(function($) {
         resizeLayout();
        trebol_isotope_percentage();
        $('.lazy').lazy({
           effect: "fadeIn",
           effectTime: 400,
           threshold: 0
        });
    });

    jQuery(window).on( 'resize', function() {
        resizeLayout();
        trebol_isotope_percentage();
    }).resize();

    jQuery(window).load(function(){
        trebol_isotope();
        trebol_isotope_percentage();
        $('.lazy').lazy({
           effect: "fadeIn",
           effectTime: 400,
           threshold: 0
        });
    });
})(jQuery);