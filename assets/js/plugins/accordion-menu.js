( function( $ ) {
    $( document ).ready(function() {
        $('#mobile-primary > ul  li.has-sub').append('<span class="holder"></span>');
        $('#mobile-primary li.has-sub > span.holder').on('click', function(e){
            e.preventDefault();
            var element = $(this).parent('li');
            if (element.hasClass('open')) {
                element.removeClass('open');
                element.find('li').removeClass('open');
                element.find('ul').slideUp();
            }
            else {
                element.addClass('open');
                element.children('ul').slideDown();
                element.siblings('li').children('ul').slideUp();
                element.siblings('li').removeClass('open');
                element.siblings('li').find('li').removeClass('open');
                element.siblings('li').find('ul').slideUp();
            }
        });

        $("#vertical-primary").each(function(){
            $('#vertical-primary > ul  li.has-sub').append('<span class="holder"></span>');
            $('#vertical-primary li.has-sub > span.holder').on('click', function(e){
                e.preventDefault();
                var element = $(this).parent('li');
                if (element.hasClass('open')) {
                    element.removeClass('open');
                    element.find('li').removeClass('open');
                    element.find('ul').slideUp();
                }
                else {
                    element.addClass('open');
                    element.children('ul').slideDown();
                    element.siblings('li').children('ul').slideUp();
                    element.siblings('li').removeClass('open');
                    element.siblings('li').find('li').removeClass('open');
                    element.siblings('li').find('ul').slideUp();
                }
            });
        });

    });
} )( jQuery );
